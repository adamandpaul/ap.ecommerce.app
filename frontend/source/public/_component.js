/* globals document,console */


export let component = () => {
    console.log('component loaded');
};


/**
 * Add two numbers together and displays on page
 * @arg {int} a First number
 * @arg {int} b Second number
 * @return {int} Sum of the two numbers
*/
export function sum_and_display(a, b) {
    let sum = a + b;
    document.write(`<h1 id=sum>${sum}</h1>`);
}
