/* globals test,expect,document */

// NOTE run this test from frontend dir via:
//     NODE_ENV=production node_modules/.bin/jest
// NODE_ENV is needed so that babel is used (Jest doesn't understand modules)

import {sum_and_display} from './_component.js';

test('checks sum_and_display works', () => {
    sum_and_display(1, 2);
    expect(document.querySelector('#sum').innerHTML).toEqual("3");
});
