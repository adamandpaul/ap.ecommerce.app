# -*- coding: utf-8 -*-

from .. import domainlib
from . import orm
from datetime import datetime
from datetime import timedelta
from pyramid.decorator import reify
from pyramid.security import Allow
from pyramid.security import DENY_ALL
from uuid import UUID
from uuid import uuid4


INFO = 20


class LogEntry(domainlib.DomainSQLAlchemyRecord):
    """A log message"""

    __acl__ = [
        (Allow, 'role:site-administrator', ['admin-view',
                                            'admin-export']),
        DENY_ALL,
    ]

    @reify
    def title(self):
        return f'{self._record.timestamp}: {self._record.component}'

    @reify
    def description(self):
        if self._record.level == 20:
            level = 'INFO'
        else:
            level = str(self._record.level)
        return f'{level}: {self._record.message}'

    @classmethod
    def get_info_key_descriptions(cls):
        descriptions = super().get_info_key_descriptions().copy()
        descriptions['timestamp'] = 'Timestamp'
        descriptions['level_text'] = 'Log Level'
        descriptions['message'] = 'Message'
        return descriptions

    @reify
    def info(self):
        """Return common values"""
        record = self._record
        info = super().info.copy()
        info['timestamp'] = record.timestamp
        info['level_text'] = 'INFO' if record.level == INFO else str(record.level)
        info['message'] = record.message
        return info

    record_type = orm.LogEntry
    id_fields = ('log_entry_id',)


class LogEntryCollection(domainlib.DomainSQLAlchemyRecordCollection):
    """A collection of log messages"""

    __acl__ = [
        (Allow, 'role:site-administrator', ['admin-view',
                                            'admin-export']),
        DENY_ALL,
    ]

    child_type = LogEntry

    default_order_by_fields = ['timestamp desc']

    def name_from_child(self, child):
        """Return the child name"""
        return str(child.id['log_entry_id'])

    def id_from_name(self, name: str) -> dict:
        try:
            log_entry_id = UUID(name)
        except ValueError as e:
            raise TypeError('Name is not a valid uuid') from e
        if name != str(log_entry_id):
            raise TypeError('Incorrectly formatted uuid')
        return {'log_entry_id': log_entry_id}

    def iter_recent_logs(self):
        """Return logs within the past day"""
        before = datetime.utcnow() - timedelta(days=1)
        q = self.query()
        q = q.filter(orm.LogEntry.timestamp >= before)
        q = q.order_by(orm.LogEntry.timestamp.desc())
        for record in q:
            yield self.child_from_record(record)


class ComponentLogger(LogEntryCollection):
    """A logger for a component"""

    title = 'Logger'

    @property
    def description(self):
        return f'Component logger for component key "{self.component}"'

    def __init__(self, parent=None, name: str = None, component: str = None):
        """Create a component logger"""
        super().__init__(parent=parent, name=name)
        assert isinstance(component, str)
        self.component = component

    def query(self, *args, **kwargs):
        return super().query(*args, **kwargs).filter_by(component=self.component)

    def log_info(self, message):
        """Log an info level message to the log_entry table"""
        record = orm.LogEntry(log_entry_id=uuid4(),
                              timestamp=datetime.utcnow(),
                              level=INFO,
                              component=self.component,
                              message=message)
        self.acquire.db_session.add(record)
