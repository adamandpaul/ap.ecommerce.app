# -*- coding: utf-8 -*-

from .. import domain
from ..testing import POSTGRESQL_LAYER

import sqlalchemy
import unittest


class MockRegistry(object):
    settings = {}


class MockRequest(object):
    db_session = None
    registry = None


class DomainUnitTest(unittest.TestCase):
    """A domain unit test"""

    layer = POSTGRESQL_LAYER

    site_factory = domain.Site
    pyramid_registry_settings = {}

    def setUp(self):
        db_engine = sqlalchemy.create_engine(self.layer['dsn'])
        db_session = sqlalchemy.orm.sessionmaker(bind=db_engine)()
        self.db_session = db_session

        request = MockRequest()
        request.registry = MockRegistry()
        request.registry.settings = self.pyramid_registry_settings
        request.db_session = db_session
        self.request = request
        site = self.site_factory(parent=None, name='Site', request=request)

        site.init_database(db_engine)
        self.site = site
