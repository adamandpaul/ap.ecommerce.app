# -*- coding: utf-8 -*-

from . import orm
from .user import User
from datetime import datetime
from uuid import UUID

import unittest


class TestUser(unittest.TestCase):

    def setUp(self):
        self.user_record = orm.User(user_email='user@noemail.adamandpaul.biz',
                                    user_uuid=UUID('2af8be60-9e8b-11e7-8d4d-5254002bacad'))

    def test_user_info_and_display_name(self):
        user = User(record=self.user_record)
        expected_user_info = {
            'user_email': 'user@noemail.adamandpaul.biz',
            'user_uuid': UUID('2af8be60-9e8b-11e7-8d4d-5254002bacad'),
        }
        self.assertEqual(user.info, expected_user_info)
        self.assertEqual(user.title, 'User: user@noemail.adamandpaul.biz')

    def test_bad_chars_valid_user_id(self):
        bad_chars = '][{}|#?/:<>%`\x00\x10\x7f'
        for c in bad_chars:
            emails = [
                f'{c}aa@example.com',
                f'a{c}a@example.com',
                f'aa@ex{c}ample.com',
                f'aa@example.co{c}m',
            ]
            for email in emails:
                self.assertFalse(User.is_user_email_valid(email))

    def test_bad_valid_user_id(self):
        emails = [
            '',
            ' ',
            '@',
            ' @example.com',
            '@example.com',
            'user@ex ample.com',
            'user@examplle.c om',
            'user1@',
            'javascript:alert("helo")@example.com',
            'aa@' + ('b' * 249) + '.co',  # an email of 255 chars should be invalid
            f'aa@"example.com"',
        ]
        for email in emails:
            self.assertFalse(User.is_user_email_valid(email))

    def test_valid_user_id(self):
        emails = [
            'email@example.com',
            '"my funny quoted email"@example.com',
            '"email@user"@example.com',
            'email+tag@example.com',
            '"我吃了一个苹果"@example.com',
            'user@我吃了一个苹果.com',
            'aa@' + ('b' * 248) + '.co',  # an email of 254 chars should be valid
        ]
        for email in emails:
            self.assertTrue(User.is_user_email_valid(email))

    def test_hash_api_token(self):
        # since api tokens are heigh entropy secrets, toeksn are mearly sha256 secrets
        data = [
            ('', b"\xe3\xb0\xc4B\x98\xfc\x1c\x14\x9a\xfb\xf4\xc8\x99o\xb9$'\xaeA\xe4d\x9b\x93L\xa4\x95\x99\x1bxR\xb8U"),
            ('abc', b'\xbax\x16\xbf\x8f\x01\xcf\xeaAA@\xde]\xae"#\xb0\x03a\xa3\x96\x17z\x9c\xb4\x10\xffa\xf2\x00\x15\xad'),
            (
                'MxS3x0k0CI-qBv-3q0ab4qmNBCgWOoR4nh8m24iNc29BeKRKyyxO-DBBgQKdBqVp_sYWWnn6FOLFoQaai7Jgsg',
                b'N\xe6\xe1\xd6\xa8\x94[;\x1f\xe0\xc8Z\xec\x02\xd9\xc0\x87\x8a\xbe\x84\x8e\x18>s\x80\xfb\xeb\x8a6\xc4\xf81',
            ),
        ]
        for token, expected_hash in data:
            token_hash = User.hash_api_token(token)
            self.assertEqual(token_hash, expected_hash)

    def test_regenerate_api_token(self):

        user = User(record=self.user_record)
        assert user._record.api_token_hash is None, 'User record shoul not, by default have any api tokens'
        token_1 = user.regenerate_api_token()
        hash_1 = user._record.api_token_hash

        # Test that we get expected values
        self.assertGreater(len(token_1), 70)
        self.assertIsNotNone(token_1)
        self.assertIsNotNone(hash_1)
        self.assertIsInstance(hash_1, bytes)

        # Test that we get new values
        token_2 = user.regenerate_api_token()
        hash_2 = user._record.api_token_hash
        self.assertNotEqual(token_1, token_2)
        self.assertNotEqual(hash_1, hash_2)

    def test_initiate_password_reset(self):
        user = User(record=self.user_record)
        self.assertIsNone(user._record.password_reset_token)
        self.assertIsNone(user._record.password_reset_expiry)

        # Works the first time
        user.initiate_password_reset()
        self.assertEqual(len(user._record.password_reset_token), 32)
        self.assertIsInstance(user._record.password_reset_expiry, datetime)

        # Works on subsequent times too
        user.initiate_password_reset()
        self.assertEqual(len(user._record.password_reset_token), 32)
        self.assertIsInstance(user._record.password_reset_expiry, datetime)
