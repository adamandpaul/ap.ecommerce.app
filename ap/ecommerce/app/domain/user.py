# -*- coding: utf-8 -*-

from .. import domainlib
from . import orm
from .logger import ComponentLogger
from ap.ecommerce.app.security import check_password
from ap.ecommerce.app.security import hash_password
from base64 import b32encode
from datetime import datetime
from datetime import timedelta
from pyramid.decorator import reify
from pyramid.security import Allow
from pyramid.security import DENY_ALL

import hashlib
import re
import secrets


# VALID_USER_EMAIL checks for a semi-validish email. Of most concern
# it will fail to validate emails with unsafe url charactors
VALID_USER_EMAIL_EXPRESSION = (
    '^'
    '[^][{}|#?/:<>%`\\\\\x00-\x1f\x7f ]'  # The first letter: url unsafe chars + space
    '[^][{}|#?/:<>%`\\\\\x00-\x1f\x7f]*'  # Letters up to @ symbol: url unstafe chars
    '@'  # an @ symbol
    '[^]"\'@[{}|#?/:<>%`\\\\\x00-\x1f\x7f ]+'  # Domain parts: url unsafe chars + space + quotes + @
    '\\.'  # a dot
    '[^]"\'@[{}|#?/:<>%`\\\\\x00-\x1f\x7f ]{2,}'  # The top level: url unsafe chars + sapece + quotes + @
    '$'
)
VALID_USER_EMAIL = re.compile(VALID_USER_EMAIL_EXPRESSION)


class User(domainlib.DomainSQLAlchemyRecord):
    """Object for manainge a user"""

    def __acl__(self):
        return [
            (Allow, 'role:site-administrator', ['admin-view',
                                                'admin-workflow',
                                                'admin-user-regenerate-api-token',
                                                'manage']),
            (Allow, f'user_email:{self.email}', ['manage']),
            DENY_ALL,
        ]

    record_type = orm.User
    id_fields = ('user_uuid',)

    @reify
    def title(self):
        return f'User: {self._record.user_email}'

    @reify
    def description(self):
        return f'{self.workflow_state} user. User UUID: {self.id["user_uuid"]}'

    @reify
    def user_uuid(self):
        return self.id['user_uuid']

    @reify
    def email(self):
        return self._record.user_email

    @reify
    def password_reset_expiry(self):
        return self._record.password_reset_expiry

    @reify
    def password_reset_token(self):
        return self._record.password_reset_token

    @reify
    def password_reset_confirm_url(self):
        base32_email = b32encode(self.email.encode('utf-8')).decode('utf-8')
        token = self.password_reset_token
        return f'/password-reset/{base32_email}/token/{token}'

    @classmethod
    def get_info_key_descriptions(cls):
        descriptions = super().get_info_key_descriptions().copy()
        descriptions['user_uuid'] = 'User UUID'
        descriptions['user_email'] = 'User Email'
        return descriptions

    @reify
    def info(self):
        info = super().info.copy()
        info['user_uuid'] = self.user_uuid
        info['user_email'] = self.email
        return info

    @classmethod
    def is_user_email_valid(cls, user_email):
        """Test if a potential user_email is a valid (safe) email

        Because we use email's in our url scheme the VALID_USER_EMAIL regular expression filters
        out potential unsafe charactors.

        Args:
            user_email (str): The value to be tested

        Returns:
            bool: True if the user_email was valid. Otherwise False
        """
        if len(user_email) > 254:
            return False
        return VALID_USER_EMAIL.match(user_email) is not None

    @classmethod
    def hash_api_token(cls, token):
        """Hash an api token

        Returns:
            bytes: The hash of the token
        """
        encoded = token.encode('utf-8')
        token_hash = hashlib.sha256(encoded)  # No need to salt since we are dealing with a high entropy secret
        digest = token_hash.digest()
        return digest

    def regenerate_api_token(self):
        """Generate an api token of heigh entropy storing it's hash in the field api_token_hash.
        Any previous token is thus invalidated.

        Returns:
            string: A freshly baked new api token
        """
        token = secrets.token_urlsafe(64)
        token_hash = self.hash_api_token(token)
        self._record.api_token_hash = token_hash
        return token

    def initiate_password_reset(self):
        """Generates and sets the password_reset_token and
        password_reset_expiry fields, if a password reset is not yet in
        progress or has already expired.

        :raises: User.ResetInProgressError if an active reset is in progress
        """
        record = self._record
        now = datetime.utcnow()
        token = secrets.token_urlsafe(24)
        record.password_reset_token = token
        record.password_reset_expiry = now + timedelta(days=1)

    workflow_default_state = 'active'
    workflow_transitions = {
        'ban': {
            'from': ('active',),
            'to': 'banned',
        },
        'reinstate': {
            'from': ('banned',),
            'to': 'active',
        },
    }

    def get_logger(self):
        """Get the logger for this member"""
        component = f'user:{self.id["user_uuid"]}'
        logger = ComponentLogger(parent=self, name='logger', component=component)
        return logger
    get_logger.tinterface_factory_for = 'logger'

    def password_match(self, password):
        """check if a provided password (unhashed), matches the user password"""
        return check_password(password, self._record.password)

    def set_password(self, password):
        self._record.password = hash_password(password)
        self._record.password_reset_token = None
        self._record.password_reset_expiry = None
        self.logger.info(f'Password updated for {self.email}')
