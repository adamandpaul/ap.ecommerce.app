# -*- coding: utf-8 -*-
"""The Shop domain/context object for the ecommerce site"""

from .. import domainlib
from .. import utils
from . import orm
from .users import Users
from datetime import date
from pyramid.security import Allow
from pyramid.security import Authenticated
from pyramid.security import DENY_ALL

import logging
import re
import sqlalchemy
import sqlalchemy.sql.expression


logger = logging.getLogger('ap.ecommerce.app')


class Site(domainlib.Site):
    """The Site object

    Attributes:
        db_session: An SQLAlchemy database session object
    """

    __name__ = None
    __parent__ = None
    __acl__ = [
        (Allow, 'role:site-implementer', ['admin-setup']),
        (Allow, 'role:site-administrator', ['admin-view', 'admin-docs']),
        DENY_ALL,
    ]

    sqlalchemy_bases = [orm.Base]

    def __init__(self, parent=None, name: str = None, request=None, settings=None):
        """Initialize the site domain object
        """
        super().__init__(parent=parent, name=name, request=request, settings=settings)
        self.pyramid_registry = getattr(request, 'registry', None)

    def set_redirect(self, path, query_string, redirect):
        """Set a redirect for a given url.

        The query string is re-ordered to be alphebetical. UTM paramitors are striped.

        Args:
            path (str): The path to match for a redirecting request
            query_string (str): The query string to match for a redirecting request
        """
        query_string = query_string or ''
        assert f'{path}?{query_string}'.strip('?') != redirect.strip('?')
        query_string = utils.normalize_query_string(query_string, ignore_prefixes=['utm_'])
        assert f'{path}?{query_string}'.strip('?') != redirect.strip('?')
        redirect = orm.Redirect(request_path=path,
                                request_query_string=query_string,
                                redirect_to=redirect)
        self.db_session.merge(redirect)

    def get_redirect(self, path, query_string):
        """Retreive a redirect for a given path and query string

        Args:
            path (str): The path to match for a redirecting request
            query_string (str): The query string to match for a redirecting request
        """
        if not self.get_database_is_inited():
            return None
        query_string = utils.normalize_query_string(query_string, ignore_prefixes=['utm_'])
        redirects = self.db_session.query(orm.Redirect) \
            .filter_by(request_path=path) \
            .filter(sqlalchemy.sql.expression.text(":input_query_string LIKE (request_query_string || '%')").bindparams(input_query_string=query_string))

        redirects = sorted(redirects, key=lambda r: len(r.request_query_string) * -1)  # longest query string match first
        if len(redirects) > 0:
            return redirects[0].redirect_to
        else:
            return None

    def list_redirects(self):
        """Return a list of current redirects"""
        return self.db_session.query(orm.Redirect)

    def get_database_is_inited(self):
        """Find out if we have a database"""
        return 'user' in self.db_session.bind.table_names()

    def user_principals_for_user_email(self, user_email):
        """Return a list of principals for a given user email"""

        principals = set()

        # Configuration provided principals
        registry = self.get_request().registry
        if registry['emergency_admin_authorization_login'] is not None:
            if re.match(registry['emergency_admin_authorization_login'], user_email) is not None:
                if date.today() < registry['emergency_admin_authorization_valid_before']:
                    principals.add('user_email:' + user_email)  # add the user email since they are an emergency admin
                    principals.add('group:emergency-admin-users')
                    principals.add(Authenticated)

        user = self.get_users().get_user_by_email(user_email)
        if user is not None and user.workflow_state != 'banned':
            principals.add('user_email:' + user_email)
            principals.add('user_uuid:' + str(user.id['user_uuid']))
            principals.add(Authenticated)

        return list(principals)

    def role_principals_for_principals(self, principals):

        role_principals = []

        # Append roles for group:emergency-admin-users
        if 'group:emergency-admin-users' in principals:
            role_principals.append('role:site-administrator')
            role_principals.append('role:site-implementer')

        return role_principals

    def get_user_display_name(self, user_uuid):
        """Return the user display name"""
        user = self.get_users().get(user_uuid)
        if user is None:
            return None
        else:
            return user.email

    @classmethod
    def init_database(cls, db_engine):
        """Initialize database tables"""
        for base in cls.sqlalchemy_bases:
            base.metadata.create_all(db_engine)

    @classmethod
    def database_ddl(cls, db_engine):
        """Return the ddl of the current database"""
        dialect = db_engine.dialect
        sql_parts = []

        def metadata_dump(sql, *args, **vargs):
            sql_data = sql.compile(dialect=dialect)
            sql_parts.append(sql_data)

        tmp_engine = sqlalchemy.create_engine(db_engine.url, strategy='mock', executor=metadata_dump)
        for base in cls.sqlalchemy_bases:
            base.metadata.create_all(tmp_engine)
        return '\n'.join([str(s) for s in sql_parts])

    def get_users(self):
        return Users(parent=self, name='users')
    get_users.tinterface_factory_for = 'users'
