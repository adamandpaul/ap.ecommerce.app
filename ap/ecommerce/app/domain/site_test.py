# -*- coding: utf-8 -*-

from .. import domain
from .testing import DomainUnitTest

import sqlalchemy
import sqlalchemy.orm
import unittest
import unittest.mock


class TestInitSite(unittest.TestCase):

    def test_system_init_database(self):

        db_engine = sqlalchemy.create_engine('sqlite:///:memory:')
        db_session = sqlalchemy.orm.sessionmaker(bind=db_engine)()
        site = domain.Site()
        site.db_session = db_session
        self.assertEqual(site.db_session, db_session)

        self.assertFalse(db_engine.dialect.has_table(db_engine, 'log_entry'))
        self.assertFalse(site.get_database_is_inited())
        site.init_database(db_engine)
        self.assertTrue(db_engine.dialect.has_table(db_engine, 'log_entry'))
        self.assertTrue(site.get_database_is_inited())

        self.assertIsNone(site.__name__)
        self.assertIsNone(site.__parent__)


class TestSite(DomainUnitTest):

    def test_site_contents(self):
        self.assertIsInstance(self.site['users'], domain.Users)


class TestSiteRedirect(unittest.TestCase):

    def test_set_redirect(self):

        with unittest.mock.patch('ap.ecommerce.app.utils.normalize_query_string') as normalize_query_string:
            normalize_query_string.return_value = 'normalized=1'
            site = domain.Site(request=unittest.mock.MagicMock())
            site.set_redirect('/part1/part2', 'b=2&a=1', 'https://localhost')
            normalize_query_string.assert_called_with('b=2&a=1', ignore_prefixes=['utm_'])
            record = site.db_session.merge.call_args[0][0]
            self.assertEqual(record.request_path, '/part1/part2')
            self.assertEqual(record.request_query_string, 'normalized=1')
            self.assertEqual(record.redirect_to, 'https://localhost')
