# -*- coding: utf-8 -*-
"""Exceptions which  can be raised"""

import ap.ecommerce.app.exc


class DomainError(ap.ecommerce.app.exc.APECommerceAppError):
    """Error in the ap.ecommerce.app domain"""


class DomainInstanceNotFound(DomainError):
    """Raised when a domain object instance can not be found"""


class ContractNotFound(DomainInstanceNotFound):
    """DomainINstanceNotFound for Contracts"""


class CreateUserError(DomainError):
    """An error raised when trying to create a user"""


class CreateUserErrorUserExists(CreateUserError):
    """The user already exists when trying to create a new user"""


class CreateUserErrorInvalidUserEmail(CreateUserError):
    """The user already exists when trying to create a new user"""


class WorkflowActionError(DomainError):
    """An error occured during a workflow action"""


class WorkflowUnknownActionError(DomainError):
    """Unknown action for workflow"""


class WorkflowIllegalTransitionError(DomainError):
    """A transition was not allowed from the current state"""
