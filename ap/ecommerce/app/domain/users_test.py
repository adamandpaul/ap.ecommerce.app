# -*- coding: utf-8 -*-

from .. import domain
from .testing import DomainUnitTest
from uuid import UUID


class TestUsers(DomainUnitTest):

    def test_add_user(self):
        users = domain.Users(self.site)
        user = users.add('user@noemail.adamandpaul.biz')
        user_info = user.info
        self.assertEqual(user_info['user_email'], 'user@noemail.adamandpaul.biz')
        self.assertIsInstance(user_info['user_uuid'], UUID)

    def test_can_not_add_user_with_same_email(self):
        users = domain.Users(parent=self.site)
        users.add('user@noemail.adamandpaul.biz')
        with self.assertRaises(domain.exc.CreateUserErrorUserExists):
            users.add('user@noemail.adamandpaul.biz')

    def test_bad_user_email_raises_exception(self):
        users = domain.Users(parent=self.site)
        with self.assertRaises(domain.exc.CreateUserErrorInvalidUserEmail):
            users.add('')

    def test_get_user_by_api_token(self):
        users = domain.Users(parent=self.site)
        user = users.add('user@noemail.adamandpaul.biz')
        users.add('user_2@noemail.adamandpaul.biz')

        token = user.regenerate_api_token()
        user = users.get_user_by_api_token(token)
        self.assertEqual(user.email, 'user@noemail.adamandpaul.biz')
