# -*- coding: utf-8 -*-

from .exc import WorkflowIllegalTransitionError
from .exc import WorkflowUnknownActionError
from datetime import datetime
from uuid import UUID
from zope.interface import Interface


class RecordBackedDomain(object):
    """Abstract domain object whith common behaviours of
    an object wiht a record behind it
    """

    def __init__(self, record, db_session=None):
        self._record = record
        if db_session is not None:
            self._db_session = db_session

    _id_field = None

    _info_keys = [
    ]

    _publication_workflow_field = None
    _publication_workflow_transitions = {
        'publish': {
            'from': ('unpublished',),
            'to': 'published',
        },
    }

    @property
    def info(self):
        """Get object info

        Returns:
            dict: Dictionary containing object info
        """
        info = {}
        for key in self._info_keys:
            info[key] = getattr(self._record, key, None)
        return info

    @property
    def jsonable_info(self):
        """dict: Returns the info dict that is convertable to a JSON string"""
        obj = {}
        for key, value in self.info.items():
            if isinstance(value, UUID):
                value = str(value)
            if isinstance(value, datetime):
                value = value.isoformat()
            if isinstance(value, bytes):
                value = value.hex()
            obj[key] = value
        return obj

    @property
    def id(self):
        """"Returns the id of the record. If _id_field is a tuple a tuple of fields will be retunrd"""
        if isinstance(self._id_field, str):
            return getattr(self._record, self._id_field)
        elif isinstance(self._id_field, tuple):
            value = []
            for attribute in self._id_field:
                value.append(getattr(self._record, attribute))
            value = tuple(value)
            return value
        else:
            raise AttributeError('id')

    def save_from_dict(self, data):
        """Save data to the record"""
        for key, value in data.items():
            setattr(self._record, key, value)

    def workflow_action(self, action):
        """Do a workflow action on this object"""
        if self._publication_workflow_field is None:
            raise Exception('No _plication_workflow_field is defined')
        current_state = getattr(self._record, self._publication_workflow_field)
        transition = self._publication_workflow_transitions.get(action, None)
        if transition is None:
            raise WorkflowUnknownActionError(f'Unknown action {action} on an instance of {self.__class__.__name__}.')
        if current_state not in transition['from']:
            raise WorkflowIllegalTransitionError(f'Can not {action} on an instance of {self.__class__.__name__} in the state {current_state}')
        setattr(self._record, self._publication_workflow_field, transition['to'])


class DynamicDict(object):
    """A class wich allows the results of taged methods to be exposed as
    dictionary items
    """

    _dynamic_dict_item_tag_name = '_dynamic_dict'
    _dynamic_dict_item_map = None

    def __getitem__(self, key):
        if len(key) == 0:
            raise KeyError(f'Item not found {key}')
        if key[0] == '_':
            raise KeyError(f'Item not found {key}')
        target = getattr(self, key, None)
        if target is None:
            # Check other items on self to see if we have a tag
            # of different name
            for attribute_name in dir(self):
                potential_target = getattr(self, attribute_name, None)
                potential_tag = getattr(potential_target, self._dynamic_dict_item_tag_name, None)
                if potential_tag == key:
                    target = potential_target
                    break
            if target is None:
                raise KeyError(f'Item not found {key}')
        if not callable(target):
            raise KeyError(f'Item not found {key}')
        tag = getattr(target, self._dynamic_dict_item_tag_name, None)
        if tag != key:
            raise KeyError(f'Item not found {key}')
        return target()

    @classmethod
    def _item(cls, func_or_key):
        """Tag an function so it's results can be
        exposed as a diction item from instances of DynamicDict

        Args:
            func (function or str): The function to tag or the name of the key
                for the function in that case return a wrapper function
        """
        # Get Key
        if isinstance(func_or_key, str) and (not callable(func_or_key)):
            func = None
            key = func_or_key
        else:
            func = func_or_key
            key = func.__name__
        assert len(key) > 0

        def annotate_func(inner_func):
            assert getattr(inner_func, cls._dynamic_dict_item_tag_name, None) is None
            setattr(inner_func, cls._dynamic_dict_item_tag_name, key)
            return inner_func

        if func is None:
            return annotate_func
        else:
            return annotate_func(func)


class LocationAwareSetter(object):
    """Class to set the parent and name attributes on items
    returned from getitem
    """

    def __getitem__(self, key):
        item = super().__getitem__(key)
        item.__parent__ = self
        item.__name__ = key
        return item


class IWTFormEditable(Interface):
    """Marker interface to declare a domain as WTFormEditable"""
