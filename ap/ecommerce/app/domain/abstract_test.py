# -*- coding: utf-8 -*-

from .abstract import DynamicDict
from .abstract import LocationAwareSetter
from .abstract import RecordBackedDomain
from .exc import WorkflowIllegalTransitionError
from .exc import WorkflowUnknownActionError
from datetime import datetime
from uuid import UUID

import unittest


class TestRecordBackedDomain(unittest.TestCase):

    class MyDomain(RecordBackedDomain):
        _id_field = 'record_id'
        _info_keys = [
            'colour',
            'colour_uuid',
            'timestamp',
            'image',
            'published_state',
        ]
        _publication_workflow_field = 'published_state'
        _publication_workflow_transitions = {
            **RecordBackedDomain._publication_workflow_transitions,
            'warp': {
                'from': 'unknown',
                'to': 'existance',
            },
        }

    class MyMultiKeyDomain(MyDomain):
        _id_field = ('record_id', 'colour_uuid')

    class MyRecord(object):
        record_id = 'abc'
        colour = 'blue'
        colour_uuid = UUID('4b07232e-a429-11e7-83d0-5254002bacad')
        timestamp = datetime(2016, 4, 17, 12, 0)
        image = b'myblah image'
        published_state = 'unpublished'

    def test_record_backed_domain(self):
        my_domain = self.MyDomain(self.MyRecord())
        my_domain_info = my_domain.info
        self.assertEqual(my_domain_info['colour'], 'blue')
        self.assertEqual(my_domain_info['colour_uuid'], UUID('4b07232e-a429-11e7-83d0-5254002bacad'))
        self.assertEqual(my_domain.id, 'abc')

        jsonable_info = my_domain.jsonable_info
        self.assertEqual(jsonable_info['colour_uuid'], '4b07232e-a429-11e7-83d0-5254002bacad')
        self.assertEqual(jsonable_info['timestamp'], '2016-04-17T12:00:00')
        self.assertEqual(jsonable_info['image'], '6d79626c616820696d616765')

    def test_record_backed_domain_with_db_session(self):
        db_session = object()
        my_domain = self.MyDomain(self.MyRecord(), db_session)
        self.assertIs(my_domain._db_session, db_session)

    def test_record_backed_domain_multi_primary_key(self):
        my_domain = self.MyMultiKeyDomain(self.MyRecord())
        self.assertEqual(my_domain.id, ('abc', UUID('4b07232e-a429-11e7-83d0-5254002bacad')))

    def test_save_from_dict(self):
        record = self.MyRecord()
        my_domain = self.MyDomain(record, object())
        my_domain.save_from_dict({'colour': 'red'})
        self.assertEqual(record.colour, 'red')

    def test_workflow_action(self):
        record = self.MyRecord()
        my_domain = self.MyDomain(record, object())
        my_domain.workflow_action('publish')
        self.assertEqual(my_domain.info['published_state'], 'published')

    def test_workflow_action_invalid_action(self):
        record = self.MyRecord()
        my_domain = self.MyDomain(record, object())
        with self.assertRaises(WorkflowUnknownActionError):
            my_domain.workflow_action('jump')

    def test_workflow_action_illegal_transition(self):
        record = self.MyRecord()
        my_domain = self.MyDomain(record, object())
        with self.assertRaises(WorkflowIllegalTransitionError):
            my_domain.workflow_action('warp')


class TestDynamicDict(unittest.TestCase):

    def test_dynamic_dict(self):

        item_a = object()
        _item_b = object()
        item_c = object()

        class MyClass(DynamicDict):

            colour = 'blue'

            @DynamicDict._item
            def item_a(self):
                return item_a

            @DynamicDict._item
            def _item_b(self):
                return _item_b

            @DynamicDict._item('item-c')
            def item_c(self):
                return item_c

            def _private_method(self):
                return 'blah'

            def other_method(self):
                return 'blah'

        my_class = MyClass()
        self.assertIs(my_class['item_a'], item_a)
        self.assertIsNot(my_class['item_a'], _item_b)
        self.assertIs(my_class['item-c'], item_c)
        with self.assertRaises(KeyError):
            my_class['_item_b']
        with self.assertRaises(KeyError):
            my_class['_private_method']
        with self.assertRaises(KeyError):
            my_class['other_method']
        with self.assertRaises(KeyError):
            my_class['item_c']


class TestLocationAwareSetter(unittest.TestCase):

    def test_location_aware_setter(self):

        # Setup domain and it's sub object

        class MyDomain(LocationAwareSetter, dict):
            pass

        my_domain = MyDomain()

        class MySubOjbect(object):
            pass
        my_domain['sub-object'] = MySubOjbect()

        # Test that the sub object is maked with parent and name attributes
        sub_object = my_domain['sub-object']
        self.assertIs(sub_object.__parent__, my_domain)
        self.assertEqual(sub_object.__name__, 'sub-object')
