# -*- coding: utf-8 -*-

from .. import domainlib
from . import exc
from . import orm
from .user import User
from pyramid.security import Allow
from pyramid.security import DENY_ALL
from uuid import UUID
from uuid import uuid4

import wtforms


def validate_user_email(form, field):
    """Validate an email"""
    if not User.is_user_email_valid(field.data):
        raise wtforms.validators.ValidationError('Invalid User Email')


class WTFormsUsersAdd(wtforms.Form):
    """Add a new user"""

    user_email = wtforms.StringField('User Email', [wtforms.validators.Length(max=1024),
                                                    wtforms.validators.InputRequired(),
                                                    validate_user_email])


class Users(domainlib.DomainSQLAlchemyRecordCollection):
    """The folder/container for users"""

    __acl__ = [
        (Allow, 'role:site-administrator', ['admin-view',
                                            'admin-add',
                                            'admin-export']),
        DENY_ALL,
    ]

    child_type = User

    title = 'Users'
    description = 'Users are identifiable entities by an email address and have capabilites to login'
    wtforms_collection_add = WTFormsUsersAdd

    def name_from_child(self, child):
        """Return the child name"""
        return str(child.id['user_uuid'])

    def id_from_name(self, name: str) -> dict:
        try:
            user_uuid = UUID(name)
        except ValueError as e:
            raise TypeError('Name is not a valid user_uuid') from e
        if name != str(user_uuid):
            raise TypeError('Incorrectly formatted user_uuid')
        return {'user_uuid': user_uuid}

    def get_user_by_email(self, user_email: str):
        """Return a user from a given email address"""
        db_session = self.acquire.db_session
        user_query = db_session.query(orm.User) \
            .filter_by(user_email=user_email)
        record = user_query.one_or_none()
        if record is not None:
            return self.child_from_record(record)
        return None

    def add(self, user_email):
        """Create a user record and returen the user domain object associated with it

        Args:
            user_email (str): A valid user id which is an email
        """
        if not User.is_user_email_valid(user_email):
            raise exc.CreateUserErrorInvalidUserEmail()
        db_session = self.acquire.db_session
        user_exists_query = db_session.query(orm.User) \
            .filter_by(user_email=user_email) \
            .exists()
        user_exists = db_session.query(user_exists_query).scalar()
        if user_exists:
            raise exc.CreateUserErrorUserExists()
        else:
            user_record = orm.User(user_email=user_email,
                                   user_uuid=uuid4())
            db_session.add(user_record)
            user = self.child_from_record(user_record)
            user.logger.info(f'Created user for email {user_email}')
            return user

    def get_user_by_api_token(self, token):
        """Return a user for a given api token

        Args:
            token (str): The token to use to find the user

        Returns:
            User: The user for a given token
            None: If the user is not found
        """
        db_session = self.acquire.db_session
        user = None
        if len(token) > 70:  # Don't bother with short tokens
            token_hash = User.hash_api_token(token)

            # Retreive the user by token_hash
            record = db_session.query(orm.User) \
                .filter_by(api_token_hash=token_hash) \
                .one_or_none()
            if record is not None:
                user = self.child_from_record(record)

        return user
