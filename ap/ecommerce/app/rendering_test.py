# -*- coding: utf-8 -*-

from . import rendering
from .testing import PyramidConfigTestCase
from .testing import PyramidViewTestCase
from unittest.mock import call
from unittest.mock import Mock

import pyramid.testing


class TestRendering(PyramidViewTestCase):

    def setUp(self):
        self.render_globals = {'colour': 'blue'}
        self.expected_render_output = 'my zope blue template\n'
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_template_loader(self):
        registry = self.request.registry
        registry['templates'] = {'theme': 'ap.ecommerce.app:rendering_test_templates/theme.pt'}
        loader = rendering.TemplateLoader(registry)
        template = loader['theme']
        render_output = template(**self.render_globals)
        self.assertEqual(render_output, self.expected_render_output)

    def test_inject_templates(self):
        renderer_globals = {**self.render_globals, 'request': self.request}
        rendering.inject_templates(renderer_globals)
        self.assertIsInstance(renderer_globals['templates'], rendering.TemplateLoader)


class TestRenderingConfig(PyramidConfigTestCase):

    def test_includeme(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.add_directive = Mock()
            config.add_subscriber = Mock()
            rendering.includeme(config)
            self.assertIsInstance(config.registry['templates'], dict)
            config.add_directive.assert_called_with('register_template_layer', rendering.register_template_layer)
            config.add_subscriber.assert_has_calls([
                call(rendering.inject_templates, pyramid.events.BeforeRender),
                call(rendering.inject_tools, pyramid.events.BeforeRender),
            ])

    def test_register_template_layer_dir(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.include('ap.ecommerce.app.rendering')
            config.register_template_layer('ap.ecommerce.app:rendering_test_templates')
            config.register_template_layer('ap.ecommerce.app:rendering_test_templates', prefix='alt_')
            self.assertEqual(config.registry['templates'], {
                'theme': 'ap.ecommerce.app:rendering_test_templates/theme.pt',
                'alt_theme': 'ap.ecommerce.app:rendering_test_templates/theme.pt'})

            # Test that re-include doesn't wipe tempaltes
            rendering.includeme(config)
            self.assertEqual(config.registry['templates'], {
                'theme': 'ap.ecommerce.app:rendering_test_templates/theme.pt',
                'alt_theme': 'ap.ecommerce.app:rendering_test_templates/theme.pt'})
