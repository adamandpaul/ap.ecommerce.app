# -*- coding: utf-8 -*-
"""Sessions and authorization configuration for pyramid"""

from .exc import ConfigurationError
from datetime import datetime
from datetime import timedelta
from pyramid.authentication import extract_http_basic_credentials
from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.httpexceptions import HTTPForbidden
from pyramid.interfaces import IAuthenticationPolicy
from pyramid.security import Authenticated
from pyramid.security import Everyone
from uuid import uuid4
from zope.interface import implementer

import bcrypt
import binascii
import jwt
import logging
import pyramid.csrf
import pyramid.events
import pyramid_nacl_session


def hash_password(pw):
    pwhash = bcrypt.hashpw(pw.encode('utf8'), bcrypt.gensalt())
    return pwhash.decode('utf8')


def check_password(pw, hashed_pw):
    if not pw or not hashed_pw:
        return False
    else:
        expected_hash = hashed_pw.encode('utf8')
        return bcrypt.checkpw(pw.encode('utf8'), expected_hash)


logger = logging.getLogger('ap.ecommerce.app')


@implementer(IAuthenticationPolicy)
class BrowserSessionAuthenticationPolicy(SessionAuthenticationPolicy):
    """Authentication policy for on page browser sessions"""

    def unauthenticated_userid(self, request):
        """Extract the userid from the session object"""
        request.auth_method = 'session'
        return request.session.get(self.userid_key, None)


@implementer(IAuthenticationPolicy)
class TokenAuthenticationPolicy(object):
    """Authentication policy for API based requests"""

    def unauthenticated_userid_from_api_token(self, request):
        """Extract a userid from an api token"""
        credentials = extract_http_basic_credentials(request)
        if credentials is None:
            return None

        token = credentials.username
        if len(token) > 70:
            user = request.site['users'].get_user_by_api_token(token)
            if user is not None:
                request.auth_method = 'api-token'
                return user.info['user_email']

        return None

    def unauthenticated_userid_from_jwt_token(self, request):
        """Extract a userid from a jwt token"""
        claims = request.jwt_claims
        if claims is None:
            return None

        # Check for the claim of an access token
        if 'access' in claims.get('aud', []):
            request.auth_method = 'jwt-token'
            return claims.get('sub', None)
        else:
            return None

    def unauthenticated_userid(self, request):
        """Extract an userid from an API Token"""
        return (
            self.unauthenticated_userid_from_api_token(request)
            or self.unauthenticated_userid_from_jwt_token(request)
            or None
        )

    def authenticated_userid(self, request):
        raise NotImplementedError()

    def effective_principals(self, request, userid=None):
        raise NotImplementedError()

    def remember(self, request, userid, **kw):
        raise NotImplementedError()

    def forget(self, request):
        raise NotImplementedError()


@implementer(IAuthenticationPolicy)
class AuthenticationPolicy(object):
    """Global authentication policy"""

    def __init__(self):
        self.token_policy = TokenAuthenticationPolicy()
        self.browser_session_policy = BrowserSessionAuthenticationPolicy()

    def policy(self, request):
        if request.domain.startswith('api.'):
            return self.token_policy
        else:
            return self.browser_session_policy

    def unauthenticated_userid(self, request):
        """Proxy unauthenticated_userid method to the auth policy for this request"""
        request.auth_method = None
        policy = self.policy(request)
        request.auth_policy = policy.__class__.__name__
        return self.policy(request).unauthenticated_userid(request)

    def remember(self, request, userid, **kw):
        """Proxy remember method to the auth policy for this request"""
        self.policy(request).remember(request, userid, **kw)

    def forget(self, request):
        """Proxy forget method to the auth policy for this request"""
        self.policy(request).forget(request)

    def authenticated_userid(self, request):
        userid = self.unauthenticated_userid(request)
        if Authenticated in self.effective_principals(request, userid=userid):
            return userid
        else:
            return None

    def effective_principals(self, request, userid=None):
        """Set the effective principals"""
        userid = userid or self.unauthenticated_userid(request)
        principals = [Everyone]

        if userid is not None:
            assert '@' in userid, 'User id must be emailish'
            user_email = userid
            site = request.site
            principals += site.user_principals_for_user_email(user_email)
            principals += site.role_principals_for_principals(principals)

        else:
            pass
        return principals


def get_user(request):
    """Return the user for a request"""
    user = None
    userid = request.authenticated_userid
    if userid is not None and '@' in userid:
        user_email = userid
        user = request.site['users'].get_user_by_email(user_email)
    return user


def should_check_csrf(request):
    """Determine if the csrf token should be checked"""
    return not request.domain.startswith('api.')


def check_origin(event):
    """If this is not a "safe method" and the browser has kindly given us an origin header
    then check that the origin is a match for  allowed origins
    """
    request = event.request
    if request.domain.startswith('api.'):
        return
    if request.method.upper() in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
        return
    allowed_origins = request.registry['allowed_origins']
    if '*' in allowed_origins:
        return
    origin = request.headers.get('Origin', None)
    if origin is None:
        return
    if origin in allowed_origins:
        return
    raise HTTPForbidden()


def get_jwt_claims(request):
    """Return the JSON web token claim from the request object.

    Only supports public/private key pair forms of JWT and must have
    request.registry.jwt_public_key and request.registry_jwt_algorithm defined.

    A registry.jwt_leeway (timedelta) can be defined. By default it is 10 seconds

    Args:
        request: A pyramid request object

    Returns:
        dict: The claims dictionary if a verified JWT was found
        None: Indicats that there was not valid JWT token given
    """

    # Check that we have a public key
    public_key = request.registry['jwt_public_key']
    algorithm = request.registry['jwt_algorithm']
    if not public_key or not algorithm:
        return None
    leeway = request.registry['jwt_leeway']

    # Extract raw token
    auth_type, token = request.authorization or (None, None)
    if auth_type != 'Bearer':
        return None
    if token is None:
        return None

    claims = jwt.decode(token,
                        key=public_key,
                        algorithms=[algorithm],
                        leeway=leeway,
                        options={'verify_aud': False})  # we verify the aud claim in the authentication policy

    return claims


def generate_jwt(request, **claims):
    """Generate a JSON Web Token (JWT) with the given claims.

    THe token generated contains the claims signed with request.registry.private_key
    using the algorithm request.registry.algorithm

    Returns:
        str: The encoded and signed json web token
    """
    private_key = request.registry['jwt_private_key']
    algorithm = request.registry['jwt_algorithm']
    assert private_key is not None
    assert algorithm is not None
    token_bytes = jwt.encode(claims,
                             key=private_key,
                             algorithm=algorithm)
    return token_bytes.decode()


def get_esession(request):
    """Wrap the default session with a session with initialized values
    """
    session = request.session
    if 'session_uuid' not in request.session:
        session['session_uuid'] = str(uuid4())
    pyramid.csrf.get_csrf_token(request)
    return session


def sync_session_info_cookies(event):
    """Event handler after render. If there is an esession generated but the
    cookies are not present for Javascript to use, then create those cookies
    """
    request = event.request
    response = event.response
    if 200 <= response.status_code <= 399:  # only inject on successful responses
        session_info_fields = [
            ('session_uuid', 'session_uuid'),
            ('csrf_token', '_csrft_'),
        ]
        for cookie_name, session_key in session_info_fields:
            cookie_value = request.cookies.get(cookie_name, None)
            try:
                session_value = request.session[session_key]
            except KeyError:
                # No session value, delete any assosciated cookie
                if cookie_value is not None:
                    response.delete_cookie(cookie_name)
            else:
                # There was a session value, update any cooke to match
                if cookie_value != str(session_value):
                    response.set_cookie(cookie_name, session_value, secure=not request.registry['is_development'])


def includeme(config):
    """Configure pyramid to use ACL authorization and use sessions"""

    settings = config.get_settings()
    config.include('pyramid_zcml')
    registry = config.registry

    # Setup allowed_origins
    registry['allowed_origins'] = []
    for token in settings['ap.ecommerce.allowed_origins'].split():
        token = token.strip()
        if token != '':
            registry['allowed_origins'].append(token)

    # Setup CSRF
    config.set_default_csrf_options(callback=should_check_csrf)

    # Authorization Policy
    config.set_authorization_policy(ACLAuthorizationPolicy())

    # JSON Web Token
    registry['jwt_private_key'] = settings.get('ap.ecommerce.jwt_private_key', None)
    registry['jwt_public_key'] = settings.get('ap.ecommerce.jwt_public_key', None)
    registry['jwt_algorithm'] = settings.get('ap.ecommerce.jwt_algorithm', None)
    registry['jwt_leeway'] = timedelta(seconds=int(settings.get('ap.ecommerce.jwt_leeway', None) or 10))
    registry['jwt_access_ttl'] = timedelta(seconds=int(settings.get('ap.ecommerce.jwt_access_ttl', None) or 60 * 60 * 24))
    registry['jwt_refresh_ttl'] = timedelta(seconds=int(settings.get('ap.ecommerce.jwt_refresh_ttl', None) or 60 * 60 * 24 * 365))

    config.add_request_method(get_jwt_claims,
                              'jwt_claims',
                              reify=True)
    config.add_request_method(generate_jwt,
                              'generate_jwt')

    # Sessions
    session_secret = binascii.unhexlify(settings['pyramid_nacl_session.session_secret'].strip())
    session_max_age = int(settings.get('pyramid_nacl_session.max_age', 1200))
    session_reissue_time = int(settings.get('pyramid_nacl_session.reissue_time', 60))
    session_factory = pyramid_nacl_session.EncryptedCookieSessionFactory(session_secret,
                                                                         secure=not registry.get('is_development', False),
                                                                         httponly=True,
                                                                         max_age=session_max_age,
                                                                         timeout=session_max_age,
                                                                         reissue_time=session_reissue_time)
    config.set_session_factory(session_factory)
    config.add_request_method(get_esession,
                              'esession',
                              reify=True)

    # Configure emergency admin
    emergency_admin_authorization_login = settings.get('ap.ecommerce.emergency_admin_authorization_login', '').strip()
    emergency_admin_authorization_valid_before = settings.get('ap.ecommerce.emergency_admin_authorization_valid_before', '').strip()
    if len(emergency_admin_authorization_login) > 0:
        try:
            emergency_admin_authorization_valid_before = datetime.strptime(emergency_admin_authorization_valid_before, '%Y-%m-%d').date()
        except ValueError as e:
            raise ConfigurationError('emergency admin authorization login set wihtout a valid "valid before" date set') from e
    else:
        emergency_admin_authorization_login = None
        emergency_admin_authorization_valid_before = None
    registry['emergency_admin_authorization_login'] = emergency_admin_authorization_login
    registry['emergency_admin_authorization_valid_before'] = emergency_admin_authorization_valid_before

    # Setup authenitcation
    authentication_policy = AuthenticationPolicy()
    config.set_authentication_policy(authentication_policy)
    config.add_request_method(get_user, 'user', reify=True)

    # Setup Logins
    config.include('velruse.providers.google_oauth2')
    config.add_google_oauth2_login_from_settings()

    # Add check origin NewRequest Subscriber
    config.add_subscriber(check_origin,
                          iface=pyramid.events.NewRequest)

    # Add sync session_info_cookies NewResponse subscriber
    config.add_subscriber(sync_session_info_cookies,
                          iface=pyramid.events.NewResponse)
