# -*- coding: utf-8 -*-


def view(context, request):
    return {
        'regenerate_api_token_url': request.resource_url(context, 'regenerate-api-token', route_name='admin'),
    }


def regenerate_api_token(context, request):
    if request.method == 'POST':
        token = context.regenerate_api_token()
    else:
        token = None
    return {
        'api_token': token,
    }
