# -*- coding: utf-8 -*-


def list_redirects(context, request):
    return {
        'redirects': context.list_redirects(),
    }
