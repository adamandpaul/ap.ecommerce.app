# -*- coding:utf-8 -*-


from ..testing import PyramidViewTestCase
from . import domain_edit
from unittest.mock import MagicMock
from webob.multidict import MultiDict

import wtforms


class TestDomainEdit(PyramidViewTestCase):

    class Domain(object):
        domain_title = 'Domain Object'
        info = {'field1': 'green'}

        class DomainEditForm(wtforms.Form):
            field1 = wtforms.StringField('field1', [wtforms.validators.Length(max=5)])

        wtforms_edit_form_factory = DomainEditForm

        def sasve_from_dict(self, data):
            raise NotImplementedError()

    def setUp(self):
        super().setUp()
        self.context = self.Domain()
        self.request.POST = MultiDict()

    def test_domain_edit_pristine(self):
        self.request.method = 'GET'
        view_data = domain_edit.wtform_edit(self.context, self.request)
        self.assertEqual(view_data['form']['field1'].data, 'green')
        self.assertEqual(view_data['form_workflow'], 'pristine')
        self.assertEqual(view_data['domain_title'], 'Domain Object')

    def test_domain_edit_error(self):
        self.request.method = 'POST'
        self.request.POST['field1'] = 'green-too-long'
        view_data = domain_edit.wtform_edit(self.context, self.request)
        self.assertEqual(view_data['form']['field1'].data, 'green-too-long')
        self.assertEqual(view_data['form_workflow'], 'error')

    def test_domain_edit_save(self):
        self.context.save_from_dict = MagicMock()
        self.request.method = 'POST'
        self.request.POST['field1'] = 'blue'
        view_data = domain_edit.wtform_edit(self.context, self.request)
        self.assertEqual(view_data['form_workflow'], 'saved')
        self.context.save_from_dict.assert_called_once_with({'field1': 'blue'})
