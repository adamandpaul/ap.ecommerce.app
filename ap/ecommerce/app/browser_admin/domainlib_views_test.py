#  -*- coding:utf-8 -*-

from . import domainlib_views
from unittest import TestCase
from unittest.mock import MagicMock
from webob.multidict import MultiDict

import wtforms


class TestDomainCollectionView(TestCase):

    def setUp(self):
        self.request = MagicMock()
        self.context = MagicMock()
        self.view = domainlib_views.DomainCollectionView(self.context, self.request)

    def test_collection_page(self):

        class Criteria(wtforms.Form):
            size = wtforms.StringField(default='small')

        self.context.wtforms_collection_criteria = Criteria
        self.request.GET = MultiDict({'criteria-size': 'big', 'page': 2})
        page = self.view.collection_page
        form_data = page['filter_form'].data
        self.assertEqual(form_data['page_size'], 50)
        self.assertEqual(form_data['page'], 2)
        self.assertEqual(form_data['criteria']['size'], 'big')
