# -*- coding: utf-8 -*-

from .. import browser_admin
from ..testing import PyramidViewTestCase


class TestUserViews(PyramidViewTestCase):

    def test_view(self):
        user = self.site['users'].add('user1@noemail.adamandpaul.biz')
        view = browser_admin.user.view(user, self.request)
        self.assertEqual(view['regenerate_api_token_url'], f'http://example.com/admin/users/{user.id["user_uuid"]}/regenerate-api-token')

    def test_regenerate_api_token(self):
        user = self.site['users'].add('user1@noemail.adamandpaul.biz')
        self.request.method = 'POST'
        view = browser_admin.user.regenerate_api_token(user, self.request)
        token = view['api_token']
        self.assertGreater(len(token), 70)
