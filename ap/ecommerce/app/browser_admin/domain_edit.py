# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPNotFound
from webob.multidict import MultiDict


def wtform_edit(context, request):
    """Edit a domain object which defins _wtforms_edit_schema"""
    form = None
    form_workflow = None
    if not hasattr(context, 'wtforms_edit_form_factory'):
        raise HTTPNotFound()
    if request.method == 'GET':
        form = context.wtforms_edit_form_factory(MultiDict(context.info))
        form_workflow = 'pristine'
    elif request.method == 'POST':
        form = context.wtforms_edit_form_factory(request.POST)
        if form.validate():
            form_workflow = 'saved'
            context.save_from_dict(form.data)
            form = context.wtforms_edit_form_factory(MultiDict(context.info))
        else:
            form_workflow = 'error'
    else:
        raise HTTPNotFound()
    return {
        'form': form,
        'form_workflow': form_workflow,
        'domain_title': getattr(context, 'domain_title', context.__class__.__name__),
    }
