# -*- coding:utf-8 -*-

from .. import domainlib
from datetime import datetime
from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPFound

import io
import wtforms


class WTFormsCollectionView(wtforms.Form):
    page_size = wtforms.IntegerField('Items Per Page',
                                     [wtforms.validators.NumberRange(1, 1000)],
                                     default=50)
    page = wtforms.IntegerField('Page',
                                [wtforms.validators.NumberRange(min=1)],
                                default=1)


class DomainBaseView(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request

    @reify
    def url(self):
        return self.request.resource_url(self.context, route_name='admin')

    @reify
    def properties(self):
        """A list of properties"""
        properties = []
        info_key_descriptions = self.context.get_info_key_descriptions()
        for key, value in self.context.info_admin_profile.items():
            key_name = info_key_descriptions.get(key, key)
            properties.append((key_name, value))
        return properties

    @reify
    def workflow_actions(self):
        """List of available workflow actions"""
        current_state = self.context.workflow_state
        workflow_actions = []
        for name, state in self.context.workflow_transitions.items():
            if current_state in state['from']:
                workflow_actions.append(name)
        return workflow_actions

    @reify
    def breadcrumbs(self):
        """A list of resource objects from the root"""
        return list(reversed(list(self.context.iter_ancestors())))

    @reify
    def view_links(self):
        """List of dictionaries of views {'href': 'https:/...', 'text': 'My View'}"""
        return []

    @reify
    def recent_logs(self):
        """Return a list of recent logs related to this object"""
        context = self.context
        if not hasattr(context, 'get_logger'):
            return []
        logger = context.get_logger()
        if not hasattr(logger, 'iter_recent_logs'):
            return []
        return [l.info for l in logger.iter_recent_logs()]

    def view(self):
        return {}

    def workflow_action(self):
        action = self.request.params.get('action', None)
        self.context.workflow_action(action)
        return HTTPFound(self.url)


class DomainCollectionView(DomainBaseView):

    @reify
    def collection_page(self):

        class Form(WTFormsCollectionView):
            criteria = wtforms.FormField(self.context.wtforms_collection_criteria)

        form = Form(self.request.GET)
        if form.validate():
            page = form.page.data
            page_size = form.page_size.data
            form_criteria = form.criteria

            criteria = self.context.criteria_from_wtforms_collection_criteria(form_criteria)
            try:
                data = self.context.filter(criteria=criteria,
                                           limit=page_size,
                                           offset=page_size * (page - 1))
            except domainlib.exc.DomainCollectionNotListable:
                return {
                    'filter_form': form,
                    'filter_form_state': 'valid',
                    'data': None,
                    'listable': False,
                }
            return {
                'filter_form': form,
                'filter_form_state': 'valid',
                'data': data,
                'listable': True,
            }
        else:
            return {
                'filter_form': form,
                'filter_form_state': 'error',
                'data': None,
                'listable': None,
            }

    @reify
    def add_form(self):
        """Get the add form"""
        if self.request.method == 'POST':
            form = self.context.wtforms_collection_add(self.request.POST)
            form.validate()
            return form
        else:
            return self.context.wtforms_collection_add()

    @reify
    def add_form_workflow(self):
        """Return the form workflow"""
        if self.request.method == 'POST':
            form = self.context.wtforms_collection_add(self.request.POST)
            if form.validate():
                return 'valid'
            else:
                return 'error'
        else:
            return 'pristine'

    def add(self):
        """Form to use to add a new item"""
        if self.request.method == 'POST':
            form = self.add_form
            if form.validate():
                child = self.context.add(**form.data)
                child_url = self.request.resource_url(child, route_name='admin')
                return HTTPFound(child_url)
        return {
            'form': self.add_form,
            'form_workflow': self.add_form_workflow,
        }

    def export_csv(self):

        # Get data
        stream = io.StringIO()
        self.context.write_csv(stream)

        # Return response
        response = self.request.response
        response.text = stream.getvalue()
        response.headers['content-type'] = 'text/csv'
        timestamp = datetime.utcnow().isoformat()
        file_name = f'{self.context.title} {timestamp}.csv'
        response.headers['content-disposition'] = f'attachment; filename="{file_name}"'
        return response


class DomainRecordView(DomainBaseView):
    """Record View Allows for editing record properties"""

    @reify
    def edit_form(self):
        """Get the current edit form"""
        if self.request.method == 'POST':
            form = self.context.wtforms_record_edit(self.request.POST)
            form.validate()
            return form
        else:
            return self.context.wtforms_record_edit()

    def edit(self):
        """Edit an object's properties"""
        if self.request.method == 'POST':
            form = self.edit_form
            if form.validate():
                self.context.edit(**form.data)
                return HTTPFound(self.url)
            else:
                form_workflow = 'error'
        else:
            form_workflow = 'pristine'
        return {
            'form': self.edit_form,
            'form_workflow': form_workflow,
        }
