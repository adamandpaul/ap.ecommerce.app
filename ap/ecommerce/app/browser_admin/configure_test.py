# -*- coding: utf-8 -*-

from .. import browser_admin
from .. import domain
from ..domainlib import site as domainlib_site
from ..testing import PyramidConfigTestCase
from . import domainlib_views

import pyramid.testing


class TestBrowserAdminConfiguration(PyramidConfigTestCase):

    def test_browser_admin_configuration(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.registry.is_development = True
            config.include('ap.ecommerce.app.browser_admin')

            # subscriber IBeforeRender rendering.inject_browser_templates not tested
            # because can't find a way to introspect the presence of the callable

            # Assert Static View
            self.assert_static(config, '++static++ap.ecommerce.app.browser_admin/', 'ap.ecommerce.app.browser_admin:static/')

            # Assert the admin route
            self.assert_route(config, 'admin_root', '/admin', None)
            self.assert_route(config, 'admin', '/admin*traverse', None)

            # Assert site views
            self.assert_view(config, domainlib_site.Site, domainlib_views.DomainBaseView,
                             renderer='templates/dashboard.pt', route_name='admin_root', permission='admin-view')

            # Asser User Views
            self.assert_view(config, domain.User, browser_admin.user.regenerate_api_token,
                             route_name='admin', name='api-token', permission='admin-user-regenerate-api-token',
                             renderer='templates/user_regenerate_api_token.pt')

            # Assert Abstract Interface Views
            self.assert_view(config, domain.abstract.IWTFormEditable, browser_admin.domain_edit.wtform_edit,
                             renderer='templates/domain_wtform_edit.pt', route_name='admin', permission='admin-edit')
