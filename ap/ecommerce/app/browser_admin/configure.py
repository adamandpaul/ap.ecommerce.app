# -*- coding: utf-8 -*-


def includeme(config):
    """Pyramid configuration include"""
    config.include('pyramid_zcml')
    config.include('..rendering')
    config.register_template_layer('ap.ecommerce.app.browser_admin:templates', prefix='admin__')
    config.load_zcml('configure.zcml')
