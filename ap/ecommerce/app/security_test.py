# -*- coding: utf-8 -*-

from .exc import ConfigurationError
from ap.ecommerce.app.security import AuthenticationPolicy
from ap.ecommerce.app.security import check_origin
from ap.ecommerce.app.security import generate_jwt
from ap.ecommerce.app.security import get_jwt_claims
from ap.ecommerce.app.security import includeme
from ap.ecommerce.app.security import should_check_csrf
from ap.ecommerce.app.security import sync_session_info_cookies
from ap.ecommerce.app.security import TokenAuthenticationPolicy
from ap.ecommerce.app.testing import PyramidConfigTestCase
from ap.ecommerce.app.testing import PyramidViewTestCase
from datetime import date
from datetime import timedelta
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid_nacl_session import EncryptedCookieSessionFactory
from unittest.mock import MagicMock
from unittest.mock import Mock
from unittest.mock import patch
from uuid import UUID

import pyramid.events
import pyramid.httpexceptions
import pyramid.testing
import unittest


class TestTokenAuthenticationPolicy(unittest.TestCase):

    def test_unathenticated_userid(self):
        policy = TokenAuthenticationPolicy()
        policy.unauthenticated_userid_from_api_token = Mock(return_value=None)
        policy.unauthenticated_userid_from_jwt_token = Mock(return_value='foobar')
        request = MagicMock()
        self.assertEqual(policy.unauthenticated_userid(request),
                         'foobar')
        policy.unauthenticated_userid_from_api_token.assert_called_with(request)
        policy.unauthenticated_userid_from_jwt_token.assert_called_with(request)

    @patch('ap.ecommerce.app.security.extract_http_basic_credentials')
    def test_unauthenticated_userid_from_api_token(self, extract_http_basic_credentials):
        request = MagicMock()
        token = 'a' * 71
        credentials = extract_http_basic_credentials.return_value
        credentials.username = token
        expected_user = request.site['users'].get_user_by_api_token(token)
        policy = TokenAuthenticationPolicy()
        userid = policy.unauthenticated_userid_from_api_token(request)
        self.assertEqual(userid, expected_user.info['user_email'])

    @patch('ap.ecommerce.app.security.extract_http_basic_credentials')
    def test_unauthenticated_userid_from_api_token_no_credential(self, extract_http_basic_credentials):
        request = MagicMock()
        extract_http_basic_credentials.return_value = None
        policy = TokenAuthenticationPolicy()
        userid = policy.unauthenticated_userid_from_api_token(request)
        self.assertIsNone(userid)

    @patch('ap.ecommerce.app.security.extract_http_basic_credentials')
    def test_unauthenticated_userid_from_api_token_no_user_for_token(self, extract_http_basic_credentials):
        request = MagicMock()
        token = 'a' * 71
        credentials = extract_http_basic_credentials.return_value
        credentials.username = token
        request.site['users'].get_user_by_api_token.return_value = None
        policy = TokenAuthenticationPolicy()
        userid = policy.unauthenticated_userid_from_api_token(request)
        self.assertIsNone(userid)

    def test_unauthenticated_userid_from_jwt_token(self):
        policy = TokenAuthenticationPolicy()
        request = MagicMock()
        request.jwt_claims = {
            'sub': 'foo',
            'aud': ['access'],
        }
        userid = policy.unauthenticated_userid_from_jwt_token(request)
        self.assertEqual(userid, 'foo')

    def test_unauthenticated_userid_from_jwt_token_without_jwt(self):
        policy = TokenAuthenticationPolicy()
        request = MagicMock()
        request.jwt_claims = None
        userid = policy.unauthenticated_userid_from_jwt_token(request)
        self.assertIsNone(userid)

    def test_unauthenticated_userid_from_jwt_token_without_access(self):
        policy = TokenAuthenticationPolicy()
        request = MagicMock()
        request.jwt_claims = {
            'sub': 'foo',
            'aud': ['refresh'],
        }
        userid = policy.unauthenticated_userid_from_jwt_token(request)
        self.assertIsNone(userid)


class TestJWT(unittest.TestCase):

    @patch('jwt.decode')
    def test_get_jwt_claims(self, jwt_decode):

        request = MagicMock()
        request.registry = {}
        request.registry['jwt_public_key'] = 'pub key'
        request.registry['jwt_algorithm'] = 'myalgo'
        request.registry['jwt_leeway'] = timedelta(seconds=10)
        request.authorization = ('Bearer', 'mytoken')

        claims = get_jwt_claims(request)
        self.assertEqual(claims, jwt_decode.return_value)

        jwt_decode.assert_called_with('mytoken',
                                      key='pub key',
                                      algorithms=['myalgo'],
                                      leeway=timedelta(seconds=10),
                                      options={'verify_aud': False})

    @patch('jwt.decode')
    def test_get_jwt_claims_no_pub_key(self, jwt_decode):

        request = MagicMock()
        request.registry = {}
        request.registry['jwt_public_key'] = None
        request.registry['jwt_algorithm'] = 'myalgo'
        request.registry['jwt_leeway'] = timedelta(seconds=10)
        request.authorization = ('Bearer', 'mytoken')

        claims = get_jwt_claims(request)
        self.assertIsNone(claims)

    @patch('jwt.encode')
    def test_generate_jwt(self, jwt_encode):
        request = MagicMock()
        request.registry = {}
        request.registry['jwt_private_key'] = 'priv key'
        request.registry['jwt_algorithm'] = 'myalgo'
        request.registry['jwt_leeway'] = timedelta(seconds=10)
        token = generate_jwt(request, sub='user1')
        expected_token = jwt_encode.return_value.decode()
        self.assertEqual(token, expected_token)
        jwt_encode.assert_called_with({'sub': 'user1'}, key='priv key', algorithm='myalgo')


class TestSecurity(PyramidConfigTestCase):

    settings = {
        **PyramidConfigTestCase.settings,
        'ap.ecommerce.jwt_private_key': 'private-key-foo',
        'ap.ecommerce.jwt_public_key': 'public-key-foo',
        'ap.ecommerce.jwt_algorithm': 'rs256',
        'ap.ecommerce.jwt_leeway': '17',
        'ap.ecommerce.jwt_access_ttl': '123',
        'ap.ecommerce.jwt_refresh_ttl': '5000',
    }

    def test_security(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:

            config.add_request_method = Mock()
            config.load_zcml = Mock()
            config.add_subscriber = Mock()

            # We need to test that the session factory is indeed the one produced bye
            # EncryptedCookieSessionFactory. Unfortunatly, this isn't a class as the
            # name caseing suggests and it appears that there is no way to detect
            # that the encryption serialization settings are present on the factory
            # that is produced from introspector.get_category('session factory')
            # Therefore we create a wrapper to capture the value from EncryptedCookieSessionFactory
            # patch it into the module and test that the captured return value
            # is the session factory on our Pyramid configuration

            expected_session_factory = None

            def session_factory_wrapper(*args, **kwargs):
                nonlocal expected_session_factory
                expected_session_factory = EncryptedCookieSessionFactory(*args, **kwargs)
                return expected_session_factory

            with patch('pyramid_nacl_session.EncryptedCookieSessionFactory',
                       wraps=session_factory_wrapper) as session_factory_patch:

                includeme(config)
                introspector = config.registry.introspector

                # Check that we have configured allowed_origns
                self.assertEqual(config.registry['allowed_origins'], ['localhost'])

                # Check authorization policy
                authorization_policy_info = introspector.get_category('authorization policy')
                self.assertEqual(len(authorization_policy_info), 1)
                authorization_policy = authorization_policy_info[0]['introspectable']['policy']
                self.assertIsInstance(authorization_policy, ACLAuthorizationPolicy)

                # Check JWT setup
                self.assertEqual(config.registry['jwt_private_key'], 'private-key-foo')
                self.assertEqual(config.registry['jwt_public_key'], 'public-key-foo')
                self.assertEqual(config.registry['jwt_algorithm'], 'rs256')
                self.assertEqual(config.registry['jwt_leeway'], timedelta(seconds=17))
                self.assertEqual(config.registry['jwt_access_ttl'], timedelta(seconds=123))
                self.assertEqual(config.registry['jwt_refresh_ttl'], timedelta(seconds=5000))

                config.add_request_method.assert_any_call(get_jwt_claims,
                                                          'jwt_claims',
                                                          reify=True)
                config.add_request_method.assert_any_call(generate_jwt,
                                                          'generate_jwt')

                # Check session factory
                assert expected_session_factory is not None
                session_factory_patch.assert_called_once()
                self.assertTrue(session_factory_patch.mock_calls[0][-1]['secure'])
                session_factory_info = introspector.get_category('session factory')
                self.assertEqual(len(session_factory_info), 1)
                session_factory = session_factory_info[0]['introspectable']['factory']
                self.assertEqual(session_factory, expected_session_factory)

                # Check emergency admin values (should be None)
                self.assertIsNone(config.registry['emergency_admin_authorization_login'])
                self.assertIsNone(config.registry['emergency_admin_authorization_valid_before'])

                # Check authorization policy
                authentication_policy_info = introspector.get_category('authentication policy')
                self.assertEqual(len(authentication_policy_info), 1)
                authentication_policy = authentication_policy_info[0]['introspectable']['policy']
                self.assertIsInstance(authentication_policy, AuthenticationPolicy)

                # Check velruse google_oauth2 provider
                self.assertIn('google', config.registry.velruse_providers)

                # Check that we have registed the check_origin event subscriber
                config.add_subscriber.assert_any_call(check_origin, iface=pyramid.events.NewRequest)

                # Check taht we have registed syhnc session info cookies
                config.add_subscriber.assert_any_call(sync_session_info_cookies, iface=pyramid.events.NewResponse)


class TestSecurityWIthEmergencyAdmins(PyramidConfigTestCase):

    def test_emergency_admin(self):
        settings = self.settings.copy()
        settings['ap.ecommerce.emergency_admin_authorization_login'] = 'test-user'
        settings['ap.ecommerce.emergency_admin_authorization_valid_before'] = '2017-08-16'
        with pyramid.testing.testConfig(settings=settings) as config:
            config.include('ap.ecommerce.app.security')
            self.assertEqual(config.registry['emergency_admin_authorization_login'], 'test-user')
            self.assertEqual(config.registry['emergency_admin_authorization_valid_before'], date(2017, 8, 16))

    def test_emergency_admin_missing_date(self):
        settings = self.settings.copy()
        settings['ap.ecommerce.emergency_admin_authorization_login'] = 'test-user'
        with pyramid.testing.testConfig(settings=settings) as config:
            config.registry['is_development'] = True
            with self.assertRaises(ConfigurationError):
                config.include('ap.ecommerce.app.security')

    def test_emergency_admin_invalid_date(self):
        settings = self.settings.copy()
        settings['ap.ecommerce.emergency_admin_authorization_login'] = 'test-user'
        settings['ap.ecommerce.emergency_admin_authorization_valid_before'] = '16/08/2017'
        with pyramid.testing.testConfig(settings=settings) as config:
            with self.assertRaises(ConfigurationError):
                config.include('ap.ecommerce.app.security')


class TestCheckOrigin(PyramidViewTestCase):

    settings = {
        **PyramidViewTestCase.settings,
        'ap.ecommerce.allowed_origins': 'origin1 origin2',
    }

    def test_allowed_requests(self):
        data = [
            ('GET', 'origin1'),
            ('HEAD', 'origin1'),
            ('OPTIONS', 'origin1'),
            ('TRACE', 'origin1'),
            ('POST', 'origin1'),
            ('PUT', 'origin1'),
            ('DELETE', 'origin1'),

            ('GET', 'bad-origin1'),
            ('HEAD', 'bad-origin1'),
            ('OPTIONS', 'bad-origin1'),
            ('TRACE', 'bad-origin1'),
        ]
        for method, origin in data:
            request = self.request
            request.method = method
            request.headers['Origin'] = origin
            event = pyramid.events.NewRequest(request)
            check_origin(event)

    def test_disallowed(self):
        data = [
            ('POST', 'bad-origin1'),
            ('PUT', 'bad-origin1'),
            ('DELETE', 'bad-origin1'),
        ]
        for method, origin in data:
            request = self.request
            request.method = method
            request.headers['Origin'] = origin
            event = pyramid.events.NewRequest(request)
            with self.assertRaises(pyramid.httpexceptions.HTTPForbidden):
                check_origin(event)

    def test_absent_origin(self):
        data = [
            'GET',
            'HEAD',
            'OPTIONS',
            'TRACE',
            'POST',
            'PUT',
            'DELETE',
        ]
        for method in data:
            request = self.request
            request.method = method
            event = pyramid.events.NewRequest(request)
            check_origin(event)


class TestCheckAllowAllOrigin(PyramidViewTestCase):

    settings = {
        **PyramidViewTestCase.settings,
        'ap.ecommerce.allowed_origins': '*',
    }

    def test_allow_all(self):
        data = [
            ('GET', 'bad-origin1'),
            ('HEAD', 'bad-origin1'),
            ('OPTIONS', 'bad-origin1'),
            ('TRACE', 'bad-origin1'),
            ('POST', 'bad-origin1'),
            ('PUT', 'bad-origin1'),
            ('DELETE', 'bad-origin1'),
        ]
        for method, origin in data:
            request = self.request
            request.method = method
            request.headers['Origin'] = origin
            event = pyramid.events.NewRequest(request)
            check_origin(event)


class TestEsessionRequestMethod(PyramidViewTestCase):

    def test_request_should_have_esession_dictionary_with_initialized_values(self):
        esession = self.request.esession
        self.assertIsInstance(esession, dict)
        self.assertIsInstance(UUID(esession['session_uuid']), UUID)
        self.assertTrue(len(str(esession['_csrft_'])) > 30)


class TestShouldCheckCSRF(unittest.TestCase):

    def test_should_check_csrf_for_api_domain(self):
        request = Mock()
        request.domain = 'api.localhost'
        self.assertFalse(should_check_csrf(request))

    def test_should_check_csrf(self):
        request = Mock()
        request.domain = 'localhost'
        self.assertTrue(should_check_csrf(request))


class TestSyncSessionCookie(PyramidViewTestCase):

    def test_new_session(self):
        esession = self.request.esession
        assert len(esession['session_uuid']) > 0
        assert len(esession['_csrft_']) > 0

        response = self.request.response
        event = pyramid.events.NewResponse(self.request, response)
        sync_session_info_cookies(event)

        cookies = {}
        set_cookie_headers = response.headers.getall('set-cookie')
        for header_value in set_cookie_headers:
            value_parts = header_value.split(';')
            assignment_part = value_parts[0]
            cookie_name, cookie_value = assignment_part.split('=')
            cookies[cookie_name] = cookie_value
        self.assertEqual(cookies['session_uuid'], esession['session_uuid'])
        self.assertEqual(cookies['csrf_token'], esession['_csrft_'])

    def test_no_change_session(self):
        request = self.request
        esession = request.esession
        assert len(esession['session_uuid']) > 0
        assert len(esession['_csrft_']) > 0
        request.cookies['session_uuid'] = esession['session_uuid']
        request.cookies['csrf_token'] = esession['_csrft_']

        response = self.request.response
        event = pyramid.events.NewResponse(self.request, response)
        sync_session_info_cookies(event)

        cookies = {}
        set_cookie_headers = response.headers.getall('set-cookie')
        for header_value in set_cookie_headers:
            value_parts = header_value.split(';')
            assignment_part = value_parts[0]
            cookie_name, cookie_value = assignment_part.split('=')
            cookies[cookie_name] = cookie_value
        self.assertNotIn('session_uuid', cookies)
        self.assertNotIn('csrf_token', cookies)

    def test_delete_session(self):
        request = self.request
        esession = request.esession
        assert len(esession['session_uuid']) > 0
        assert len(esession['_csrft_']) > 0
        request.cookies['session_uuid'] = esession['session_uuid']
        request.cookies['csrf_token'] = esession['_csrft_']
        request.session.invalidate()

        response = self.request.response
        event = pyramid.events.NewResponse(self.request, response)
        sync_session_info_cookies(event)

        cookies = {}
        set_cookie_headers = response.headers.getall('set-cookie')
        for header_value in set_cookie_headers:
            value_parts = header_value.split(';')
            assignment_part = value_parts[0]
            cookie_name, cookie_value = assignment_part.split('=')
            cookies[cookie_name] = cookie_value
        self.assertEqual(cookies['session_uuid'], '')
        self.assertEqual(cookies['csrf_token'], '')

    def test_session_value_updated(self):
        request = self.request
        esession = request.esession
        assert len(esession['session_uuid']) > 0
        assert len(esession['_csrft_']) > 0
        request.cookies['session_uuid'] = esession['session_uuid']
        request.cookies['csrf_token'] = esession['_csrft_']

        esession['_csrft_'] = 'new-value'

        response = self.request.response
        event = pyramid.events.NewResponse(self.request, response)
        sync_session_info_cookies(event)

        cookies = {}
        set_cookie_headers = response.headers.getall('set-cookie')
        for header_value in set_cookie_headers:
            value_parts = header_value.split(';')
            assignment_part = value_parts[0]
            cookie_name, cookie_value = assignment_part.split('=')
            cookies[cookie_name] = cookie_value
        self.assertNotIn('session_uuid', cookies)
        self.assertEqual(cookies['csrf_token'], 'new-value')
