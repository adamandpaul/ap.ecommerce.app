# -*- coding: utf-8 -*-

from . import domain

import os
import plone.testing
import psycopg2
import psycopg2.extensions
import pyramid.request
import pyramid.testing
import shutil
import sqlalchemy
import subprocess
import tempfile
import unittest


class PyramidConfigTestCase(unittest.TestCase):
    """A base TestCase for tesing pyramid configuration"""

    settings = {
        'ap.ecommerce.is_development': 'True',
        'sqlalchemy.url': 'sqlite://',
        'velruse.google.consumer_key': 'test consumer key',
        'velruse.google.consumer_secret': 'test consumer secret',
        'pyramid_nacl_session.session_secret': '7fff281337fa1b7b1e7b19a86be50e48182f973fb8013e7d6b3fde168b4efc9d',
        'ap.ecommerce.allowed_origins': 'localhost',
    }

    site_class = domain.Site

    def _test_introspectable_category_result(self, category_result, renderer=None, permission=None,  # noqa: C901 Too complex
                view=None, request_method=None, **keys):
        """Test that a category result matches keys"""

        # Check all the keys match

        introspectable = category_result['introspectable']
        for key, value in keys.items():
            if introspectable.get(key) != value:
                return False
        introspectable_calable = introspectable.get('callable', None)

        # request_metohd is pluralized in the introspectable

        if request_method is not None:
            if introspectable.get('request_methods') != request_method:
                return False

        # Check related items match

        related = category_result['related']
        if renderer is not None:
            related_templates = [r['name'] for r in related if r.category_name == 'templates']
            if renderer not in related_templates:
                return False

        if permission is not None:
            related_permissions = [r['value'] for r in related if r.category_name == 'permissions']
            if permission not in related_permissions:
                return False

        if view is not None:
            if introspectable_calable == view:
                pass
            else:
                related_views = [r['callable'] for r in related if r.category_name == 'views']
                if view not in related_views:
                    return False

        # All checks passed this categroy_result is a match

        return True

    def _debug_introspectable_category_result(self, category_result, renderer=None, permission=None,
                                              view=None, request_method=None, **keys):

        """Print debuggable info about the category_result acordding to the params given"""

        lines = []

        # Check all the keys match

        introspectable = category_result['introspectable']
        for key, value in keys.items():
            introspectable_value = introspectable.get(key)
            lines.append(f'{key}: {introspectable_value}')
        introspectable_calable = introspectable.get('callable', None)

        # request_metohd is pluralized in the introspectable

        if request_method is not None:
            request_methods = introspectable.get('request_methods')
            lines.append(f'request_method(s): {request_methods}')

        # Check related items match

        related = category_result['related']
        if renderer is not None:
            related_templates = ', '.join([repr(r['name']) for r in related if r.category_name == 'templates'])
            lines.append(f'related_templated: {related_templates}')

        if permission is not None:
            related_permissions = ', '.join([repr(r['value']) for r in related if r.category_name == 'permissions'])
            lines.append(f'related_permissions: {related_permissions}')

        if view is not None:
            related_views = ', '.join([repr(r['callable']) for r in related if r.category_name == 'views'])
            lines.append(f'related_views: {related_views}')
            lines.append(f'introspectable_calable: {introspectable_calable}')

        return '  ' + '\n  '.join(lines)

    def assert_introspectable(self, config, category, **keys):

        # Make sure we haven't misspelt any of our predicates
        valid_keys = ['context', 'callable', 'renderer', 'name', 'route_name', 'permission',
                      'request_method', 'spec', 'pattern', 'view']
        for key in keys.keys():
            assert key in valid_keys

        introspector = config.introspector
        for item in introspector.get_category(category):
            if self._test_introspectable_category_result(item, **keys):
                # Match was found. Finish up now.
                return
            else:
                pass

        # No matching introspectable was found. Fail.
        debug_info = self._debug_assert_introspectable(config, category, **keys)
        self.fail(f'Introspectable not found {category}:\n\n{debug_info}\n  ---\n  Looking for {keys}\n')

    def _debug_assert_introspectable(self, config, category, **keys):
        introspector = config.introspector
        debug_items = []
        for item in introspector.get_category(category):
            debug_items.append(self._debug_introspectable_category_result(item, **keys))
        return '\n  ---\n'.join(debug_items)

    def assert_view(self, config, context, view, **keys):
        """Assert that a view exists"""
        self.assert_introspectable(config, 'views', context=context, view=view, **keys)

    def assert_static(self, config, name, spec):
        """Assert that a static view exists"""
        self.assert_introspectable(config, 'static views', name=name, spec=spec)

    def assert_route(self, config, name, pattern, view):
        """Assert that a route exists"""
        self.assert_introspectable(config, 'routes', name=name, pattern=pattern, view=view)


class PostgresqlLayer(plone.testing.Layer):
    """Layer for testing postgresql"""

    def setUp(self):
        # Create and run a tempory postgresql
        self['data_dir'] = tempfile.mkdtemp(dir='/tmp')  # use the tempfs mount
        self['socket_dir'] = self['data_dir']
        self['tcp_host'] = ''
        self['host'] = self['socket_dir']
        self['port'] = 5432
        self['user'] = 'testing'
        self['postgresql_prefix'] = os.environ['POSTGRESQL_PREFIX']
        self['db_count'] = 0

        subprocess.run((
            f'{os.environ["POSTGRESQL_PREFIX"]}/bin/initdb',
            '--auth', 'reject',
            '--auth-local', 'trust',
            '--pgdata', self['data_dir'],
            '--encoding', 'UTF8',
            '--username', self['user'],
        ), check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        self['process'] = subprocess.Popen((f'{os.environ["POSTGRESQL_PREFIX"]}/bin/postgres',
                                            '-D', self['data_dir'],
                                            '-p', str(self['port']),
                                            '-h', self['tcp_host'],
                                            '-k', self['socket_dir'],
                                            '-F'),  # turn off fsync. We want speed and disposability.
                                           stdin=subprocess.DEVNULL,
                                           stdout=subprocess.DEVNULL,
                                           stderr=subprocess.DEVNULL)

    def testSetUp(self):  # noqa: N802 function name 'testTareDown' should be lowercase xxx
        self['db_count'] += 1
        self['database'] = f'unittest_{self["db_count"]}'
        conn = psycopg2.connect(f'dbname=postgres user={self["user"]} host={self["host"]}')
        conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute(f'create database {self["database"]};')
        conn.commit()
        self['dsn'] = f'postgresql://{self["user"]}:@/{self["database"]}?host={self["host"]}'

    def testTearDown(self):  # noqa: N802 function name 'testTareDown' should be lowercase xxx
        # Disconnect all connections and clean up databases
        conn = psycopg2.connect(f'dbname=postgres user={self["user"]} host={self["host"]}')
        conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        cur.execute('select pg_terminate_backend(pid) '
                    'from pg_stat_activity '
                    f'where datname = \'{self["database"]}\' and pid <> pg_backend_pid() ')
        cur.execute(f'drop database {self["database"]};')
        conn.commit()

        # Clean up values
        del self['database']
        del self['dsn']

    def tearDown(self):
        """Stop postgresql process and remove data dir"""
        process = self['process']
        process.terminate()
        try:
            process.wait(timeout=10)
        except subprocess.TimeoutExpired:
            if self.poll() is None:
                process.kill()
                process.wait(timeout=10)
        shutil.rmtree(self['data_dir'])


POSTGRESQL_LAYER = PostgresqlLayer()


class PyramidViewTestCase(PyramidConfigTestCase):
    """A base TestCase for pyramid views"""

    layer = POSTGRESQL_LAYER

    settings = {
        **PyramidConfigTestCase.settings,
    }

    def setUp(self):
        self.settings = {
            **self.settings,
            'sqlalchemy.url': self.layer['dsn'],
        }
        config = pyramid.testing.setUp(settings=self.settings)
        config.include('ap.ecommerce.app')
        config.commit()
        self.config = config
        request = pyramid.testing.DummyRequest()
        pyramid.request.apply_request_extensions(request)
        request.tm.begin()
        self.request = request
        self.site_class.init_database(config.registry['db_engine'])

        site = self.site_class(request=request)
        self.site = site
        request.root = site
        request.site = site
        super().setUp()

    def tearDown(self):
        super().tearDown()
        sqlalchemy.orm.session.close_all_sessions()
        self.config.registry['db_engine'].dispose()
        pyramid.testing.tearDown()
