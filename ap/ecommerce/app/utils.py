# -*- coding: utf-8 -*-
"""Utility functions for ap.ecommerce.app egg
"""

from numbers import Number

import urllib.parse


class AnnotePermissionRequired(object):
    """Function decorator to set the permission_required attribute"""

    def __init__(self, permission):
        """Add attribute to annotate a required permission

        Args:
            permission (str): The permission to annotate
        """
        self.permission = permission

    def __call__(self, method):
        method.permission_required = self.permission
        return method


annote_permission_required = AnnotePermissionRequired


def yesish(value, default=None):
    """Determins if a value is yes"""
    if value is None:
        return default
    if isinstance(value, bool):
        return value
    if isinstance(value, Number):
        return bool(value)
    if isinstance(value, str):
        value = value.strip().lower()
        if value == '':
            return default
        if value in ('y', 'yes', 't', 'true', '1'):
            return True
        if value in ('n', 'no', 'f', 'false', '0'):
            return False
        raise TypeError('Can not determin a yesish value')
    raise TypeError('Can not determin a yesish value')


def normalize_query_string(query_string, ignore_prefixes=[]):
    """Normalize a query string by sorting it's key value pairs, also optionally
    filtering out key's which are prefixed by any values in ignore prefixes

    Args:
        query_string(str): A query string
        ignore_prefixes(list): A list of prefixes which should be discarded

    Returns:
        str: A normalized query string
    """
    query_string = query_string or ''
    query_items = urllib.parse.parse_qsl(query_string, keep_blank_values=True)
    filtered_query_items = []
    for key, value in query_items:
        keep = True
        for ignored_prefix in ignore_prefixes:
            if key.startswith(ignored_prefix):
                keep = False
                break
        if keep:
            filtered_query_items.append((key, value))
    query_items = sorted(filtered_query_items)
    query_string = urllib.parse.urlencode(query_items)
    return query_string


class DynamicDict(object):
    """A class wich allows the results of taged methods to be exposed as
    dictionary items
    """

    _dynamic_dict_item_tag_name = '_dynamic_dict'

    def __getitem__(self, key):
        if len(key) == 0:
            raise KeyError(f'Item not found {key}')
        if key[0] == '_':
            raise KeyError(f'Item not found {key}')
        target = getattr(self, key, None)
        if target is None:
            raise KeyError(f'Item not found {key}')
        if not callable(target):
            raise KeyError(f'Item not found {key}')
        tag = getattr(target, self._dynamic_dict_item_tag_name, None)
        if tag != key:
            raise KeyError(f'Item not found {key}')
        return target()

    @classmethod
    def _item(cls, func):
        """Tag an function so it's results can be
        exposed as a diction item from instances of DynamicDict

        Args:
            func (function): The function to tag
        """
        key = func.__name__
        assert len(key) > 0
        assert getattr(func, cls._dynamic_dict_item_tag_name, None) is None
        setattr(func, cls._dynamic_dict_item_tag_name, key)
        return func
