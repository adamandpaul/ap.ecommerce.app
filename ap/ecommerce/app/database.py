# -*- coding: utf-8 -*-
"""Database level pyramid configuration"""

import logging
import pyramid.events
import sqlalchemy
import zope.sqlalchemy


logger = logging.getLogger('ap.ecommerce.app')


def db_session_from_request(request):
    """Create a dbsession for a given request"""
    db_session_factory = request.registry['db_session_factory']
    db_session = db_session_factory()
    zope.sqlalchemy.register(db_session, transaction_manager=request.tm)
    return db_session


def includeme(config):
    """Configure the database"""
    # Configure database connection

    config.include('pyramid_tm')

    # Create engine and session factory
    db_engine = sqlalchemy.engine_from_config(config.get_settings(), 'sqlalchemy.')
    logger.debug(f'Database connection: {db_engine.url}')
    db_session_factory = sqlalchemy.orm.sessionmaker()
    db_session_factory.configure(bind=db_engine)
    config.registry['db_engine'] = db_engine
    config.registry['db_session_factory'] = db_session_factory

    # Add db_session to requests
    config.add_request_method(db_session_from_request,
                              'db_session',
                              reify=True)

    # Add event to ensure all the orm config mappers are loaded
    config.add_subscriber(lambda app: sqlalchemy.orm.configure_mappers(),
                          pyramid.events.ApplicationCreated)
