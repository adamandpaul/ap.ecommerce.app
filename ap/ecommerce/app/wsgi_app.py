# -*- coding: utf-8 -*-

from . import domain

import pyramid.config


def root_factory(request):
    """Get the root context object
    """
    return domain.Site(request=request)


def main(global_config, **settings):
    """Main pyramid wsgi entry point for ap.cloud.app web application
    """
    config = pyramid.config.Configurator(settings=settings,
                                         root_factory=root_factory)
    config.include('ap.ecommerce.app')
    return config.make_wsgi_app()
