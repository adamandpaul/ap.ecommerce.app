# -*- coding: utf-8 -*-

from . import domain
from .testing import PyramidConfigTestCase
from .testing import PyramidViewTestCase
from unittest.mock import Mock
from unittest.mock import patch

import ap.ecommerce.app.wsgi_app
import pyramid.router
import pyramid.testing


class TestWsgiApp(PyramidConfigTestCase):
    def test_wsgi_app(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.include = Mock()
            with patch('pyramid.config.Configurator', return_value=config):
                app = ap.ecommerce.app.wsgi_app.main(None)
            config.include.assert_any_call('ap.ecommerce.app')
            self.assertIsInstance(app, pyramid.router.Router)


class TestRootFactory(PyramidViewTestCase):
    def test_root_factory(self):
        root = ap.ecommerce.app.wsgi_app.root_factory(self.request)
        self.assertIsInstance(root, domain.Site)
