# -*- coding: utf-8 -*-

from ap.ecommerce.app.testing import PyramidConfigTestCase

import pyramid.request
import pyramid.testing
import sqlalchemy
import unittest.mock


class TestDatabaseConfiguration(PyramidConfigTestCase):

    def test_database_configure(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.include('ap.ecommerce.app.database')

            # Make sure we created a session_factory
            db_session_factory = config.registry['db_session_factory']
            self.assertIsInstance(db_session_factory, sqlalchemy.orm.session.sessionmaker)

            # Check engine
            engine = db_session_factory.kw['bind']
            self.assertEqual(str(engine.url), 'sqlite://')

            # Make sure we can get our session from the request
            # that it is registers with the tansaction manager
            # and that it works
            request = pyramid.testing.DummyRequest()
            pyramid.request.apply_request_extensions(request)
            with unittest.mock.patch('zope.sqlalchemy.register') as register_mock:
                session = request.db_session
                register_mock.assert_called_once()
            self.assertIsInstance(session, sqlalchemy.orm.session.Session)
            self.assertEqual(session.execute('select 1').first(), (1,))
