# -*- coding: utf-8 -*-


class APECommerceAppError(Exception):
    """The root error emmited by ap.ecommerce.app"""


class TestException(APECommerceAppError):
    """A testing exception used by views.testing.fail_view"""


class ConfigurationError(APECommerceAppError):
    """A an error occuring with configuration"""
