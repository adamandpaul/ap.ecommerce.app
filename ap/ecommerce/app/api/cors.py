# -*- coding:utf-8 -*-


DEFAULT_CORS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, Authorization',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Max-Age': '3600',
}


def add_headers(request, response):
    """Add CORS headers to a response object"""
    cors_headers = DEFAULT_CORS_HEADERS
    response.headers.update(cors_headers)


def new_request_handler(event):
    """Handle the new request event adding a CORS add_headers responses for particular domais

    Adds headers to domains starting with "api."
    """
    request = event.request
    if request.domain.startswith('api.'):
        request.add_response_callback(add_headers)


def preflight(context, request):
    """A pyramid view to handle OPTIONS request for preflight checks of CROS"""
    response = request.response
    response.status_code = 200
    # the add_headers callback will add appropreate cors headers to the
    return response
