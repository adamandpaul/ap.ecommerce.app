# -*- coding: utf-8 -*-


def includeme(config):
    """Configure api component"""
    config.include('pyramid_zcml')
    config.load_zcml('configure.zcml')
