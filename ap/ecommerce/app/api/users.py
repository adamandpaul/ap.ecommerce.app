# -*- coding:utf-8 -*-

from datetime import datetime
from pyramid.authentication import extract_http_basic_credentials
from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPForbidden


class GenerateJWT(object):
    """Pyramid view class which generates a JSON Web Tokens (JWT) for a user

    Two tokens are returned from this view. An access tokena and a refresh token with
    payloads as follows.

    Access Token::

        {
            'sub': 'foo@bar.com',
            'aud': ['access'],
            'iat': datetime.datetime(2019, 4, 9, 10, 22, 46, 790172),
            'iat': datetime.datetime(2019, 4, 9, 11, 22, 46, 790172),
        }

    Refresh Token::

        {
            'sub': 'foo@bar.com',
            'aud': ['refresh'],
            'iat': datetime.datetime(2019, 4, 9, 10, 22, 46, 790172),
            'iat': datetime.datetime(2020, 4, 9, 10, 22, 46, 790172),
        }

    To determin the subject of the token the view looks as:

    1. Any current pyramid authentication that has been given. But only appropreate
       for the domain. E.g. a session authentication type can't be used on an api
       domain expecting token authentication.

       This should allow access tokens or api tokens to create new tokens.

    2. Basic authencitation the request is to an api domain

    3. (not yet implemented) Refresh token on an api domain.

    New access and refresh tokens are generated on each authenticated request. A JSON object
    is returned to the client::

        {
            'result': 'ok',
            'token': 'abc.def.hij',
            'refresh_token': 'lmn.opq.rst',
        }

    """

    def __init__(self, context, request):
        self.context = context
        self.request = request

    @reify
    def sub_from_pyramid(self):
        """str or None: Return a sub claim that can be gotten from pyramid authentication"""
        request = self.request

        if request.auth_method is None:
            # We must be able to id the auth_method
            return None

        # Check that the method is allowed for the domain
        if request.domain.startswith('api.'):
            # API domain. Disallow non token authe methods
            if request.auth_method not in ('api-token', 'jwt-token'):
                return None
        else:
            # Non-API domain. Disallow token auth methods
            if request.auth_method in ('api-token', 'jwt-token'):
                return None

        return request.authenticated_userid

    @reify
    def sub_from_basic_auth(self):
        """str or None: Return a sub claim from basic autentication"""

        # Only allow basic auth on the api domain
        if not self.request.domain.startswith('api.'):
            return None

        credentials = extract_http_basic_credentials(self.request)
        if credentials is not None:
            user = self.context.get_user_by_email(credentials.username)
            if user is None:
                return None
            if user and user.workflow_state != 'active':
                return None
            if user.password_match(credentials.password):
                return user.email

        return None

    @reify
    def sub_from_jwt_refresh_token(self):
        """str or None: Return a sub claim from a jwt refresh token"""
        claims = self.request.jwt_claims
        if claims is None:
            return None
        if 'refresh' not in claims.get('aud', []):
            return None
        return claims.get('sub', None)

    @reify
    def sub(self):
        """str or None: The JWT sub (subject) claim"""
        return (self.sub_from_pyramid
                or self.sub_from_basic_auth
                or self.sub_from_jwt_refresh_token
                or None)

    @reify
    def iat(self):
        """datetime: The JWT iat (issued at) claism"""
        return datetime.utcnow()

    @reify
    def access_exp(self):
        """datetime: The JWT exp (expiry) claism for the access token"""
        return self.iat + self.request.registry['jwt_access_ttl']

    @reify
    def access_token(self):
        """str: A newly JWT access toekn"""
        return self.request.generate_jwt(sub=self.sub,
                                         aud=['access'],
                                         iat=self.iat,
                                         exp=self.access_exp)

    @reify
    def refresh_exp(self):
        """datetime: The JWT exp (expiry) claism for the refresh token"""
        return self.iat + self.request.registry['jwt_refresh_ttl']

    @reify
    def refresh_token(self):
        """str: A newly JWT refresh toekn"""
        return self.request.generate_jwt(sub=self.sub,
                                         aud=['refresh'],
                                         iat=self.iat,
                                         exp=self.refresh_exp)

    def post(self):
        """Generate JWT access and refresh tokens"""

        # Check request paramitors
        if self.request.method.lower() != 'post':
            raise HTTPForbidden('Request method not allowed')

        # We are not able to obtain an authentication
        if not self.sub:
            raise HTTPForbidden('Permission denied')

        return {
            'result': 'ok',
            'token': self.access_token,
            'refresh_token': self.refresh_token,
        }
