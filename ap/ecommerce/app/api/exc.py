# -*- coding:utf-8 -*-


def error(context, request):
    """Handle an exception"""
    title = 'An error occured'
    detail = None
    status_code = getattr(context, 'status_code', None)

    if status_code is not None and 400 <= status_code <= 499:  # only reveal the message for client errors
        title = getattr(context, 'title', None) or title
        detail = getattr(context, 'message', None) or detail

    if detail is None:
        message = title
    else:
        message = f'{title}: {detail}'

    request.response.status_code = status_code or 500

    return {
        'result': 'error',
        'message': message,
        'status_code': status_code,
    }
