# -*- coding:utf-8 -*-

from . import users
from datetime import datetime
from datetime import timedelta
from pyramid.httpexceptions import HTTPForbidden
from unittest.mock import MagicMock
from unittest.mock import patch

import unittest


class TestGenerateJWT(unittest.TestCase):

    def setUp(self):
        self.registry = {
            'jwt_access_ttl': timedelta(seconds=60),
            'jwt_refresh_ttl': timedelta(seconds=120),
        }
        self.request = MagicMock()
        self.request.method = 'post'
        self.request.domain = 'test.adamandpaul.biz'
        self.request.auth_method = None
        self.request.authenticated_userid = None
        self.request.registry = self.registry
        self.context = MagicMock()
        self.view = users.GenerateJWT(self.context,
                                      self.request)

    def test_sub_from_pyramid_should_be_none(self):
        self.assertIsNone(self.view.sub_from_pyramid)

    def test_sub_from_pyramid_should_forbid_session_user_to_api_domain(self):
        self.request.domain = 'api.test.adamandpaul.biz'
        self.request.auth_method = 'session'
        self.request.authenticated_userid = 'foo@foo'
        self.assertIsNone(self.view.sub_from_pyramid)

    def test_sub_from_pyramid_should_forbid_token_user_to_non_api_domain(self):
        self.request.auth_method = 'api-token'
        self.request.authenticated_userid = 'foo@foo'
        self.assertIsNone(self.view.sub_from_pyramid)

    def test_sub_from_pyramid_api_token(self):
        self.request.domain = 'api.test.adamandpaul.biz'
        self.request.auth_method = 'api-token'
        self.request.authenticated_userid = 'foo@foo'
        self.assertEqual(self.view.sub_from_pyramid, 'foo@foo')

    def test_sub_from_pyramid_session(self):
        self.request.auth_method = 'session'
        self.request.authenticated_userid = 'foo@foo'
        self.assertEqual(self.view.sub_from_pyramid, 'foo@foo')

    def test_sub_from_basic_auth_should_be_none(self):
        self.assertIsNone(self.view.sub_from_basic_auth)

    @patch('ap.ecommerce.app.api.users.extract_http_basic_credentials')
    def test_sub_from_basic_auth_should_be_none_for_non_api_domain(self, extract_creds):
        self.request.domain = 'test.adamandpaul.biz'
        self.assertIsNone(self.view.sub_from_basic_auth)

    @patch('ap.ecommerce.app.api.users.extract_http_basic_credentials')
    def test_sub_from_basic_auth(self, extract_creds):
        self.request.domain = 'api.test.adamandpaul.biz'
        credentials = extract_creds.return_value
        credentials.username = 'foo@bar'
        credentials.password = 'pass1234'
        user = self.context.get_user_by_email.return_value
        user.workflow_state = 'active'
        user.password_match.return_value = True
        user.email = 'foo@bar'
        sub = self.view.sub_from_basic_auth
        self.assertEqual(sub, 'foo@bar')

        extract_creds.assert_called_with(self.request)
        self.context.get_user_by_email.assert_called_with('foo@bar')
        user.password_match.assert_called_with('pass1234')

    @patch('ap.ecommerce.app.api.users.extract_http_basic_credentials')
    def test_sub_from_basic_auth_should_be_none_for_non_active_user(self, extract_creds):
        self.request.domain = 'api.test.adamandpaul.biz'
        credentials = extract_creds.return_value
        credentials.username = 'foo@bar'
        credentials.password = 'pass1234'
        user = self.context.get_user_by_email.return_value
        user.workflow_state = 'banned'
        user.password_match.return_value = True
        user.email = 'foo@bar'
        sub = self.view.sub_from_basic_auth
        self.assertIsNone(sub)

    @patch('ap.ecommerce.app.api.users.extract_http_basic_credentials')
    def test_sub_from_basic_auth_should_be_none_for_password_missmatch(self, extract_creds):
        self.request.domain = 'api.test.adamandpaul.biz'
        credentials = extract_creds.return_value
        credentials.username = 'foo@bar'
        credentials.password = 'pass1234'
        user = self.context.get_user_by_email.return_value
        user.workflow_state = 'active'
        user.password_match.return_value = False
        user.email = 'foo@bar'
        sub = self.view.sub_from_basic_auth
        self.assertIsNone(sub)

    def test_sub_from_jwt_refresh_token_should_be_none(self):
        self.request.jwt_claims = None
        self.assertIsNone(self.view.sub_from_jwt_refresh_token)

    def test_sub_from_jwt_refresh_token_should_be_none_for_no_refresh_aud(self):
        self.request.jwt_claims = {'aud': ['fooclaim'],
                                   'sub': 'blah'}
        self.assertIsNone(self.view.sub_from_jwt_refresh_token)

    def test_sub_from_jwt_refresh_token(self):
        self.request.jwt_claims = {'aud': ['refresh'],
                                   'sub': 'foo@blah'}
        self.assertEqual(self.view.sub_from_jwt_refresh_token, 'foo@blah')

    def test_sub_should_be_none(self):
        self.assertIsNone(self.view.sub)

    def test_sub(self):
        self.view.__dict__['sub_from_basic_auth'] = 'foo@blah'
        self.assertEqual(self.view.sub, 'foo@blah')

    @patch('ap.ecommerce.app.api.users.datetime')
    def test_iat(self, datetime):
        self.assertEqual(self.view.iat, datetime.utcnow.return_value)

    @patch('ap.ecommerce.app.api.users.datetime')
    def test_access_exp(self, mock_datetime):
        mock_datetime.utcnow.return_value = datetime(2000, 1, 1, 1, 0)
        self.assertEqual(self.view.access_exp,
                         datetime(2000, 1, 1, 1, 1))

    @patch('ap.ecommerce.app.api.users.datetime')
    def test_access_token(self, mock_datetime):
        mock_datetime.utcnow.return_value = datetime(2000, 1, 1, 1, 0)
        self.request.auth_method = 'session'
        self.request.authenticated_userid = 'foo@foo'
        generate_jwt = self.request.generate_jwt
        generate_jwt.return_value = 'abc.def.hij'
        token = self.view.access_token
        self.assertEqual(token, 'abc.def.hij')
        generate_jwt.assert_called_with(sub='foo@foo',
                                        aud=['access'],
                                        iat=datetime(2000, 1, 1, 1, 0),
                                        exp=datetime(2000, 1, 1, 1, 1))

    @patch('ap.ecommerce.app.api.users.datetime')
    def test_refresh_exp(self, mock_datetime):
        mock_datetime.utcnow.return_value = datetime(2000, 1, 1, 1, 0)
        self.assertEqual(self.view.refresh_exp,
                         datetime(2000, 1, 1, 1, 2))

    @patch('ap.ecommerce.app.api.users.datetime')
    def test_refresh_token(self, mock_datetime):
        mock_datetime.utcnow.return_value = datetime(2000, 1, 1, 1, 0)
        self.request.auth_method = 'session'
        self.request.authenticated_userid = 'foo@foo'
        generate_jwt = self.request.generate_jwt
        generate_jwt.return_value = 'abc.def.hij'
        token = self.view.refresh_token
        self.assertEqual(token, 'abc.def.hij')
        generate_jwt.assert_called_with(sub='foo@foo',
                                        aud=['refresh'],
                                        iat=datetime(2000, 1, 1, 1, 0),
                                        exp=datetime(2000, 1, 1, 1, 2))

    def test_post_should_forbid_bad_method(self):
        self.request.method = 'get'
        with self.assertRaises(HTTPForbidden):
            self.view.post()

    def test_post_should_forbid_none_sub(self):
        self.view.__dict__['sub'] = None
        with self.assertRaises(HTTPForbidden):
            self.view.post()

    def test_post(self):
        self.view.__dict__['sub'] = 'foo@blah'
        self.view.__dict__['access_token'] = 'abc'
        self.view.__dict__['refresh_token'] = 'def'
        message = self.view.post()
        self.assertEqual(message, {'result': 'ok',
                                   'token': 'abc',
                                   'refresh_token': 'def'})
