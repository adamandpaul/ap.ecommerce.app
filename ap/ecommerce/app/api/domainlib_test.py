# -*- coding:utf-8 -*-

from . import domainlib as domainlib_apiviews
from pyramid.httpexceptions import HTTPClientError
from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import PropertyMock

import json


class TestDomainlibBaseAPIView(TestCase):

    def setUp(self):
        self.context = MagicMock()
        self.request = MagicMock()
        self.view = domainlib_apiviews.DomainBaseAPIView(self.context, self.request)

    def test_api_properties(self):
        self.assertEqual(self.view.api_get, self.context.api_get)
        self.assertEqual(self.view.api_post, self.context.api_post)
        self.assertEqual(self.view.api_patch, self.context.api_patch)

    @patch('ap.ecommerce.app.api.domainlib.DomainBaseAPIView.api_get', new_callable=PropertyMock)
    def test_get(self, api_get_prop):
        response = self.view.get()
        self.assertEqual(response, self.view.api_get.return_value)

    @patch('ap.ecommerce.app.api.domainlib.DomainBaseAPIView.api_post', new_callable=PropertyMock)
    def test_post(self, api_post_prop):

        new_obj = MagicMock()
        new_obj.id = {'parent_id': 'foo', 'item_id': 3}
        self.view.api_post.return_value = new_obj

        self.request.json = {'foo': 'bar'}

        response = self.view.post()

        self.view.api_post.assert_called_with(foo='bar')
        self.assertEqual(response, {'result': 'ok',
                                    'parent_id': 'foo',
                                    'item_id': 3})

    @patch('ap.ecommerce.app.api.domainlib.DomainBaseAPIView.api_patch', new_callable=PropertyMock)
    def test_patch(self, api_patch_prop):
        self.request.json = {'foo': 'bar'}
        response = self.view.patch()
        self.view.api_patch.assert_called_with(foo='bar')
        self.assertEqual(response, {'result': 'ok'})

    def test_get_json(self):
        self.request.json = {'a': 1}
        self.assertEqual(self.view.get_json(),
                         {'a': 1})

    def test_get_json_decode_error(self):
        type(self.request).json = PropertyMock(side_effect=json.JSONDecodeError('foo', 'doc', 1))
        with self.assertRaises(HTTPClientError):
            self.view.get_json()
