# -*- coding:utf-8 -*-
"""apilib is a set of request handlers for the use with domainlib objects under an API situation
"""

from pyramid.httpexceptions import HTTPClientError
from pyramid.httpexceptions import HTTPNotFound

import json


class DomainBaseAPIView(object):
    """A simple rest api implementation ontop of domainlib objects"""

    __name__ = 'generic_api_view'

    def __init__(self, context, request):
        self.context = context
        self.request = request

    # Get JSON body
    def get_json(self):
        """Wrap the request.json in order to catch decode errors"""
        missing = object()
        try:
            obj = getattr(self.request, 'json', missing)
        except json.JSONDecodeError as e:
            raise HTTPClientError(f'Error decoding json ({e.lineno},{e.colno}): {e.msg}') from e

        if obj is missing:
            raise HTTPClientError('No json object on request')

        return obj

    # API get/post/patch methods

    @property
    def api_get(self):
        """Return the api_get function needed to be called"""
        return getattr(self.context, 'api_get', None)

    @property
    def api_post(self):
        """Return the api_post function needed to be called"""
        return getattr(self.context, 'api_post', None)

    @property
    def api_patch(self):
        """Return the api_patch function needed to be called"""
        return getattr(self.context, 'api_patch', None)

    def get(self):
        if self.api_get is None:
            raise HTTPNotFound()
        return self.api_get()

    def post(self):
        if self.api_post is None:
            raise HTTPNotFound()
        json = self.get_json()
        obj = self.api_post(**json)
        object_id = getattr(obj, 'id', None)
        if not isinstance(object_id, dict):
            object_id = {}
        return {
            'result': 'ok',
            **object_id,
        }

    def patch(self):
        if self.api_patch is None:
            raise HTTPNotFound()
        json = self.get_json()
        self.api_patch(**json)
        return {
            'result': 'ok',
        }
