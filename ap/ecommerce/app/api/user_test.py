# -*- coding: utf-8 -*-

from .. import api
from ..testing import PyramidViewTestCase


class TestUserAPI(PyramidViewTestCase):

    def setUp(self):
        super().setUp()
        settings = self.request.site.settings
        settings['ap.ecommerce.application_full_name'] = 'AP Ecommerce Application Framework'
        settings['ap.ecommerce.email_sender'] = 'Framework <ap@framework.com.au>'
        settings['ap.ecommerce.email_url_base'] = 'https://framework.com.au'

        users = self.request.site.get_users()
        users.add('harry@potter.co.uk')

    def test_reset_password_error_missing_user_email(self):
        self.request.json = {}
        result = api.user.password_reset(None, self.request)
        self.assertEqual(self.request.response.status, '400 Bad Request')
        self.assertDictEqual(result, {
            'result': 'error',
            'message': 'user_email must be provided, '
                       'e.g. {"user_email": "harry@potter.co.uk"}',
        })

    def test_reset_password_error_not_found(self):
        self.request.json = {
            'user_email': 'voldemort@potter.co.uk',
        }
        result = api.user.password_reset(None, self.request)
        self.assertEqual(self.request.response.status, '400 Bad Request')
        self.assertDictEqual(result, {
            'result': 'error',
            'message': 'User not found',
        })

    def test_reset_password_success(self):
        self.request.json = {
            'user_email': 'harry@potter.co.uk',
        }
        result = api.user.password_reset(None, self.request)
        self.assertEqual(self.request.response.status, '200 OK')
        self.assertDictEqual(result, {'result': 'ok'})
