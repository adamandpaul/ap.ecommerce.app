# -*- coding: utf-8 -*-
from pyramid.renderers import render
from pyramid_mailer.mailer import DummyMailer
from pyramid_mailer.mailer import Mailer
from pyramid_mailer.message import Message

import transaction


HTTP_400_BAD_REQUEST = 400


def password_reset(context, request):
    """Allow a user to start the reset password workflow

    Examples:

    1. Using the fetch API in Chrome/Firefox debug console:

        fetch(
            'http://localhost:8888/api/v1/users/@@password-reset',
            {method: 'POST', headers: {"Content-Type": "application/json"},
            body: JSON.stringify({user_email: 'harry@potter.co.uk'})}
        )
        .then(response => response.json()
        .then(data => console.log(data)));

    2. At a command line with `curl`:

        #!bin/bash
        curl http://localhost:8888/api/v1/users/@@password-reset\
         -X POST -H "Content-Type: application/json"\
         --data '{"user_email": "harry@potter.co.uk"}'

    """
    users = request.site.get_users()
    if 'user_email' not in request.json:
        request.response.status = HTTP_400_BAD_REQUEST
        return {
            'result': 'error',
            'message': 'user_email must be provided, '
                       'e.g. {"user_email": "harry@potter.co.uk"}',
        }
    user = users.get_user_by_email(request.json['user_email'])
    if not user:
        request.response.status = HTTP_400_BAD_REQUEST
        return {
            'result': 'error',
            'message': 'User not found',
        }

    user.initiate_password_reset()

    # Setup site object and mailer
    settings = request.site.settings
    if 'mail.host' in settings:
        mailer = Mailer.from_settings(settings)
    else:
        mailer = DummyMailer()

    application_full_name = settings['ap.ecommerce.application_full_name']
    url_base = settings['ap.ecommerce.email_url_base']
    subject = f'Password Reset - {application_full_name}'

    # Render email template
    password_reset_confirm_url = f'{url_base}{user.password_reset_confirm_url}'
    email_body = render('ap.ecommerce.app.api:templates/password_reset_email.pt', {
        'password_reset_confirm_url': password_reset_confirm_url,
        'title': subject,
    })

    # Construct and send message
    message = Message(
        subject=subject,
        sender=settings['ap.ecommerce.email_sender'],
        recipients=[user.email],
        html=email_body,
    )
    mailer.send(message)
    transaction.commit()
    return {'result': 'ok'}
