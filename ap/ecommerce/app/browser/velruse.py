# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound

import pyramid.security


def velruse_google_oauth2_login_complete(context, request):
    """Handle a login complete request"""
    user_email = context.profile['verifiedEmail']
    site = request.site

    # If the database is inited then add user to users table
    if site.get_database_is_inited():
        users = site['users']
        user = users.get_user_by_email(user_email)
        if user is None:
            user = users.add(user_email=user_email)
        user.logger.info(f'User logged in via google oauth2 from IP address {request.client_addr}')

    pyramid.security.remember(request, user_email)
    request.esession

    if 'role:site-administrator' in request.effective_principals:
        return HTTPFound('/admin')
    elif hasattr(request, 'on_login'):
        return request.on_login()
    else:
        return HTTPFound('/')


def velruse_authentication_denied(context, request):
    """Handle authentication denied cases"""
    return {}
