# -*- coding: utf-8 -*-
"""Views relateing to various exceptions which can be returned to a client
"""

import pyramid.httpexceptions
import urllib.parse


def error(context, request):
    """Generic error handeling"""
    if isinstance(context, pyramid.httpexceptions.HTTPException):
        request.response.status_code = context.code
    else:
        request.response.status_code = 500
    return {}


def error_check_for_redirect(context, request):
    """Check for a redirect, if one is present then redirect. Otherwise contineu with error()"""
    redirect_to = request.site.get_redirect(request.path, request.query_string)
    if redirect_to is not None:
        resolved_redirect_to = urllib.parse.urljoin(request.url, redirect_to)
        return pyramid.httpexceptions.HTTPFound(location=resolved_redirect_to)
    return error(context, request)


def server_error(context, request):
    """View for not forbiden response"""
    result = error(context, request)
    result['error_uuid'] = '39f12e17-f87e-4e26-8635-292a3710fa37'
    return result
