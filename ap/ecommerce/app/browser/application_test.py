# -*- coding: utf-8 -*-

from .. import browser
from ..exc import TestException
from ..testing import PyramidViewTestCase
from pyramid.exceptions import HTTPForbidden
from pyramid.exceptions import HTTPNotFound
from pyramid.response import Response
from pyramid.testing import DummyRequest

import pyramid.testing
import unittest


class TestExceptionViews(unittest.TestCase):

    def test_not_found(self):
        with self.assertRaises(HTTPNotFound):
            browser.application.test_not_found(None, None)

    def test_forbidden(self):
        with self.assertRaises(HTTPForbidden):
            browser.application.test_forbidden(None, None)

    def test_fail_view(self):
        with self.assertRaises(TestException):
            browser.application.test_fail_view(None, None)


class TestRobotsTxt(unittest.TestCase):

    def setUp(self):
        pyramid.testing.setUp()

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_default_robots_txt(self):
        context = None
        request = DummyRequest()
        response = browser.application.robots_txt(context, request)
        self.assertIsInstance(response, Response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'text/plain')
        self.assertEqual(response.text, 'User-agent: *\nDisallow: /')


class TestSitemapXML(PyramidViewTestCase):
    def test_site_map_xml(self):
        response = browser.application.sitemap_xml(None, self.request)
        self.assertIn('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>', response.text)


class TestCheck(unittest.TestCase):

    def test_default_robots_txt(self):
        response = browser.application.check(None, None)
        self.assertIsInstance(response, Response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'text/plain')
        self.assertEqual(response.text, '200 ok app')


class TestCheckDb(PyramidViewTestCase):

    def test_check_db_should_work_with_db(self):
        response = browser.application.check_db(None, self.request)
        self.assertIsInstance(response, Response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'text/plain')
        self.assertEqual(response.text, '200 ok db')
