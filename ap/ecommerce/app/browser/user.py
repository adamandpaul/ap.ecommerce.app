# -*- coding: utf-8 -*-
from base64 import b32decode
from datetime import datetime
from pyramid.httpexceptions import HTTPFound
from pyramid.renderers import render
from pyramid_mailer.mailer import DummyMailer
from pyramid_mailer.mailer import Mailer
from pyramid_mailer.message import Message

import transaction
import wtforms


MIN_PASSWORD_LENGTH = 8


def password_valid(form, field):
    """Password validator"""
    if len(field.data) < MIN_PASSWORD_LENGTH:
        raise wtforms.validators.ValidationError(f'Must be at least {MIN_PASSWORD_LENGTH} characters')


class WTFormsResetPassword(wtforms.Form):
    """Reset password form"""
    password = wtforms.PasswordField('Password',
                                     [password_valid])
    confirm = wtforms.PasswordField('Confirm Password',
                                    [wtforms.validators.EqualTo('password', message='Passwords must match')])


class WTFormsResetPasswordRequest(wtforms.Form):
    """Reset password request form"""
    user_email = wtforms.StringField('Email', [wtforms.validators.InputRequired(),
                                               wtforms.validators.Length(min=5, max=255)])


def password_reset_confirm(context, request):
    """Password reset confirmation view

    Allows the user to enter their new password twice, assuming they have
    provided a valid email address and token, otherwise displays a message
    informing them that the link is invalid or expired."""
    base32_email = request.matchdict['base32_email']
    token = request.matchdict['token']

    email = b32decode(base32_email.encode('utf-8')).decode('utf-8')
    user = request.site.get_users().get_user_by_email(email)
    application_full_name = request.site.settings['ap.ecommerce.application_full_name']

    errors = []
    invalid = user.password_reset_token != token
    expired = datetime.utcnow() > user.password_reset_expiry
    if invalid:
        errors.append('The reset links is not valid')
    if expired:
        errors.append('The reset links has expired')
    disabled = invalid or expired

    if request.method == 'GET':
        form = WTFormsResetPassword()
        form_workflow = 'pristine'

    elif request.method == 'POST':
        form = WTFormsResetPassword(request.POST)
        if form.validate() and not errors:
            user.set_password(form.password.data)
            return HTTPFound(f'/password-reset-complete/{base32_email}')
        else:
            form_workflow = 'error'
    else:
        raise Exception('Unsupported request method')

    if disabled:
        form.password.render_kw = {'disabled': 'disabled'}
        form.confirm.render_kw = {'disabled': 'disabled'}

    return {
        'disabled': disabled,
        'errors': errors,
        'form': form,
        'form_workflow': form_workflow,
        'title': f'{application_full_name} - Reset Password',
        'user': user,
    }


def password_reset_complete(context, request):
    """Password reset completion view"""
    settings = request.site.settings
    application_full_name = settings['ap.ecommerce.application_full_name']
    base32_email = request.matchdict['base32_email']
    email = b32decode(base32_email.encode('utf-8')).decode('utf-8')
    user = request.site.get_users().get_user_by_email(email)
    return {
        'title': f'{application_full_name} - Password Set',
        'user': user,
    }


def password_reset_request(context, request):
    """Handle a password reset request"""
    if request.method == 'GET':
        return {
            'result': None,
            'form_workflow': 'pristine',
            'form': WTFormsResetPasswordRequest(),
        }

    elif request.method == 'POST':
        form = WTFormsResetPasswordRequest(request.POST)
        if not form.validate():
            return {
                'result': 'error',
                'form_workflow': 'error',
                'form': form,
            }

        # Setup site object and mailer and options
        settings = request.site.settings
        if 'mail.host' in settings:
            mailer = Mailer.from_settings(settings)
        else:
            mailer = DummyMailer()

        email_info = {
            'application_full_name': settings['ap.ecommerce.application_full_name'],
            'url_base': settings['ap.ecommerce.email_url_base'],
            'subject': f'Password Reset - {settings["ap.ecommerce.email_url_base"]}',
            'title': f'Password Reset - {settings["ap.ecommerce.email_url_base"]}',
            'password_reset_confirm_url': None,
        }

        # See if we have a user
        user_email = form.user_email.data
        user = request.site.get_users().get_user_by_email(user_email)

        if not user:
            # Render email not found template
            email_body = render('ap.ecommerce.app.api:templates/password_reset_email_no_user.pt', email_info)

        else:
            user.initiate_password_reset()
            email_info['password_reset_confirm_url'] = f'{email_info["url_base"]}{user.password_reset_confirm_url}'

            # Render email template
            email_body = render('ap.ecommerce.app.api:templates/password_reset_email.pt', email_info)

        # Construct and send message
        message = Message(
            subject=email_info['subject'],
            sender=settings['ap.ecommerce.email_sender'],
            recipients=[user_email],
            html=email_body,
        )
        mailer.send(message)
        transaction.commit()

        return {
            'result': 'ok',
            'form': None,
            'form_workflow': None,
        }
