# -*- coding: utf-8 -*-

from ap.ecommerce.app.exc import TestException
from lxml import etree
from pyramid.exceptions import HTTPForbidden
from pyramid.exceptions import HTTPNotFound
from pyramid.response import Response

import lxml.builder


def test_not_found(context, request):
    """A view which always returns not found"""
    raise HTTPNotFound()


def test_forbidden(context, request):
    """A view which is always forbidden"""
    raise HTTPForbidden()


def test_fail_view(context, request):
    """A view which allways fails"""
    raise TestException()


def robots_txt(context, request):
    """A veiw which disallows all robots"""
    return Response(body='User-agent: *\nDisallow: /', content_type='text/plain')


def sitemap_xml(context, request):
    """A view which returns the current sitemap"""
    E = lxml.builder.ElementMaker(nsmap={None: 'http://www.sitemaps.org/schemas/sitemap/0.9'})  # noqa: N806
    urlset = E.urlset()
    body = etree.tostring(urlset, xml_declaration=True, encoding='utf-8')
    return Response(body=body, content_type='application/xml')


def check(context, request):
    """A veiw which disallows all robots"""
    return Response(body='200 ok app', content_type='text/plain')


def check_db(context, request):
    """A view which checks database connectivity"""
    result = request.db_session.execute('select 1;')
    assert result.first() == (1,)
    return Response(body='200 ok db', content_type='text/plain')
