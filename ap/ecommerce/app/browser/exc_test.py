# -*- coding: utf-8 -*-

from .. import browser
from ..testing import PyramidViewTestCase

import pyramid.httpexceptions
import pyramid.testing
import unittest


class TestExceptionHandelingViews(unittest.TestCase):

    def setUp(self):
        pyramid.testing.setUp()

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_error_result_dict_and_set_code_from_http_exception(self):
        context = pyramid.httpexceptions.HTTPException()
        context.code = 123
        request = pyramid.testing.DummyRequest()
        result = browser.exc.error(context, request)
        self.assertEqual(result, {})
        self.assertEqual(request.response.status_code, 123)

    def test_generic_error_should_result_in_500_and_error_uuid(self):
        class TestException(Exception):
            pass
        context = TestException()
        request = pyramid.testing.DummyRequest()
        result = browser.exc.server_error(context, request)
        self.assertEqual(result, {'error_uuid': '39f12e17-f87e-4e26-8635-292a3710fa37'})
        self.assertEqual(request.response.status_code, 500)


class TestExceptionsNotFound(PyramidViewTestCase):

    def test_not_found(self):
        self.site.set_redirect('/abc', 'a=b', '/target')
        context = pyramid.httpexceptions.HTTPNotFound()
        request = self.request
        request.path = '/abc'
        request.query_string = ''
        request.url = 'http://example.com/abc'
        result = browser.exc.error_check_for_redirect(context, request)
        self.assertEqual(result, {})
        self.assertEqual(request.response.status_code, 404)

    def test_redirect_found(self):
        self.site.set_redirect('/abc', 'a=b', '/target')
        context = pyramid.httpexceptions.HTTPNotFound()
        request = self.request
        request.path = '/abc'
        request.query_string = 'a=b'
        request.url = 'http://example.com/abc?a=b'
        result = browser.exc.error_check_for_redirect(context, request)
        self.assertEqual(result.location, 'http://example.com/target')
        self.assertEqual(result.status_code, 302)
