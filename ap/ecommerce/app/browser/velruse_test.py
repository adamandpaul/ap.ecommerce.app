# -*- coding: utf-8 -*-

from .. import browser
from ..testing import PyramidViewTestCase
from unittest.mock import MagicMock


class TestVelruseLoginCompleadted(PyramidViewTestCase):

    settings = {
        **PyramidViewTestCase.settings,
        'ap.ecommerce.emergency_admin_authorization_login': 'admin@noemail.adamandpaul.biz',
        'ap.ecommerce.emergency_admin_authorization_valid_before': '2100-01-01',
    }

    def setUp(self):
        super().setUp()
        self.request.client_addr = '1.1.1.1'

    def test_velruse_login_completed(self):
        self.request.on_login = MagicMock()

        class Context(object):
            profile = {'verifiedEmail': 'test@noemail.adamandpaul.biz'}

        context = Context()
        self.request.new_root = MagicMock()
        response = browser.velruse.velruse_google_oauth2_login_complete(context, self.request)
        self.assertEqual(response, self.request.on_login.return_value)
        self.assertEqual(self.request.session['auth.userid'], 'test@noemail.adamandpaul.biz')
        self.assertIn('_csrft_', self.request.session)  # This ensures that the login complete has touched the esession

    def test_velruse_login_completed_admin(self):

        class Context(object):
            profile = {'verifiedEmail': 'admin@noemail.adamandpaul.biz'}

        context = Context()
        self.request.registry.introspector = MagicMock()
        response = browser.velruse.velruse_google_oauth2_login_complete(context, self.request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.headers['location'], '/admin')

    def test_velruse_login_completed_default(self):
        class Context(object):
            profile = {'verifiedEmail': 'user@noemail.adamandpaul.biz'}

        context = Context()
        self.request.registry.introspector = MagicMock()
        response = browser.velruse.velruse_google_oauth2_login_complete(context, self.request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.headers['location'], '/')
