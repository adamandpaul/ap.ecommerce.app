# -*- coding: utf-8 -*-

from .. import browser
from ..testing import PyramidViewTestCase
from unittest.mock import MagicMock
from unittest.mock import PropertyMock


class TestTouchSession(PyramidViewTestCase):

    def test_touch_session(self):
        request = MagicMock()
        esession_propery = PropertyMock()
        type(request).esession = esession_propery
        browser.site.touch_session(None, request)
        esession_propery.assert_called_once()


class TestLogout(PyramidViewTestCase):

    def test_logout(self):
        request = self.request
        request.session['fruit'] = 'apple'
        browser.site.logout(None, request)
        self.assertNotIn('fruit', request.session)
        # we don't need to test if session info cookies are unset
        # because that is tested as part of security.sync_session_info event
