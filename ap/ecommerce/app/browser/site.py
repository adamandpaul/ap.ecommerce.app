# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound


def logout(context, request):
    """Logout"""
    request.session.invalidate()
    return HTTPFound('/')


def touch_session(context, request):
    """Check session set/reset cookies if needed"""
    request.esession
    return {}
