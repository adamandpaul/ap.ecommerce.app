# -*- coding: utf-8 -*-

from .. import browser
from .. import domain
from ..exc import TestException
from ..testing import PyramidConfigTestCase

import pyramid.testing
import velruse.providers.google_oauth2


class TestBrowserConfiguration(PyramidConfigTestCase):

    def test_browser_configuration(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.registry.is_development = True
            config.include('ap.ecommerce.app.browser')

            # Assert test fail routes
            self.assert_route(config, 'test_fail_view', '/_test/fail_view', browser.application.test_fail_view)
            self.assert_route(config, 'test_forbidden', '/_test/forbidden', browser.application.test_forbidden)
            self.assert_route(config, 'test_not_found', '/_test/not_found', browser.application.test_not_found)

            # Assert common locations
            self.assert_route(config, 'robots_txt', '/robots.txt', browser.application.robots_txt)
            self.assert_route(config, 'sitemap_xml', '/sitemap.xml', browser.application.sitemap_xml)

            # Assert check urls
            self.assert_route(config, 'check_app', '/_check/app', browser.application.check)
            self.assert_route(config, 'check_db', '/_check/db', browser.application.check_db)

            # Assert Error Responses
            self.assert_view(config, pyramid.httpexceptions.HTTPClientError, browser.exc.error, renderer='templates/error.pt')
            self.assert_view(config, TestException, browser.exc.server_error, renderer='templates/error.pt')
            self.assert_view(config, pyramid.httpexceptions.HTTPNotFound, browser.exc.error_check_for_redirect,
                             renderer='templates/error.pt')

            # Assert Site Views
            self.assert_view(config,
                             domain.Site,
                             browser.site.logout,
                             name='logout')

            self.assert_view(config,
                             velruse.providers.google_oauth2.GoogleAuthenticationComplete,
                             browser.velruse.velruse_google_oauth2_login_complete)

            self.assert_view(config,
                             domain.Site,
                             browser.site.touch_session,
                             name='touch_session')

            # Assert Template registrations
            self.assertEqual(config.registry['templates']['theme'], 'ap.ecommerce.app.browser:templates/theme.pt')


class TestBrowserProductionConfiguration(PyramidConfigTestCase):

    def test_plugin_production_configuration(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.registry['is_development'] = False
            config.include('ap.ecommerce.app.browser')
            self.assert_view(config, Exception, browser.exc.server_error, renderer='templates/error.pt')
