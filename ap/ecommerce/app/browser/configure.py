# -*- coding: utf-8 -*-


def includeme(config):
    """Pyramid include hook"""
    config.include('pyramid_zcml')
    config.include('..rendering')
    config.load_zcml('configure.zcml')
    config.register_template_layer('ap.ecommerce.app.browser:templates')
    if not config.registry.get('is_development', True):
        config.load_zcml('configure_production.zcml')
