# -*- coding: utf-8 -*-

from . import pyramid_plugin
from ap.ecommerce.app.testing import PyramidConfigTestCase
from ap.ecommerce.app.testing import PyramidViewTestCase
from datetime import datetime
from unittest import TestCase
from unittest.mock import MagicMock
from unittest.mock import Mock
from unittest.mock import patch
from uuid import UUID

import ap.ecommerce.app.exc
import pyramid.httpexceptions
import pyramid.testing
import pyramid_mailer.mailer


class TestPluginDeveloperConfiguration(PyramidConfigTestCase):

    def test_root_factory(self):
        request = MagicMock()
        root = pyramid_plugin.root_factory(request)
        self.assertEqual(root, request.site)

    @patch('pyramid.renderers.JSON')
    def test_plugin_configuration(self, JSON):  # noqa: N803

        with pyramid.testing.testConfig(settings=self.settings) as test_config:
            test_config.include('pyramid_zcml')
            test_config.include('pyramid_chameleon')
            with test_config.with_package('ap.ecommerce.app') as config:

                # Setup mocks and run
                config.add_renderer = Mock()
                config.include = Mock()
                config.add_request_method = Mock()
                config.add_google_oauth2_login_from_settings = Mock()

                # Run
                ap.ecommerce.app.pyramid_plugin.includeme(config)

                # Check that we add the json datetime adapter
                json_renderer = JSON.return_value
                json_renderer.add_adapter.assert_any_call(UUID, pyramid_plugin.json_uuid_adapter)
                json_renderer.add_adapter.assert_any_call(datetime, pyramid_plugin.json_datetime_adapter)
                config.add_renderer.assert_called_with('json', json_renderer)

                # Check that we called our sub components and plugins
                config.include.assert_any_call('pyramid_zcml')
                config.include.assert_any_call('pyramid_chameleon')
                config.include.assert_any_call('pyramid_tm')
                config.include.assert_any_call('.database')
                config.include.assert_any_call('.security')
                config.include.assert_any_call('.rendering')
                config.include.assert_any_call('.api')
                config.include.assert_any_call('.browser')
                config.include.assert_any_call('.browser_admin')

                # Check is_development
                self.assertEqual(config.registry['is_development'], True)


class TestPluginProductionConfiguration(PyramidConfigTestCase):

    settings = {
        **PyramidConfigTestCase.settings,
        'ap.ecommerce.is_development': 'False',
    }

    def test_plugin_production_configuration(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            config.include('ap.ecommerce.app')
            self.assertEqual(config.registry['is_development'], False)


class TestMailer(PyramidViewTestCase):

    def test_mailer_exists(self):
        self.assertIsInstance(self.request.mailer, pyramid_mailer.mailer.DummyMailer)


class TestMailerProduction(PyramidViewTestCase):

    settings = {
        **PyramidConfigTestCase.settings,
        'ap.ecommerce.is_development': 'False',
    }

    def test_mailer_exists(self):
        self.assertIsInstance(self.request.mailer, pyramid_mailer.mailer.Mailer)


class TestPluginProductionWithDebugToolBarConfiguration(PyramidConfigTestCase):

    settings = {
        'pyramid.includes': 'pyramid_debugtoolbar',
        'ap.ecommerce.is_development': 'False',
    }

    def test_plugin_production_configuration_should_fail_with_debug_toolbar(self):
        with pyramid.testing.testConfig(settings=self.settings) as config:
            with self.assertRaises(AssertionError):
                config.include('ap.ecommerce.app')


class TestJSONAdapters(TestCase):

    def test_json_datetime_adapter(self):
        time1 = datetime(2010, 11, 3, 7, 10)
        result = pyramid_plugin.json_datetime_adapter(time1, None)
        self.assertEqual(result, '2010-11-03T07:10:00')

    def test_json_uuid_adapter(self):
        uuid1 = UUID('1804f59c-fdcd-11e8-8602-9cb6d0dde65d')
        result = pyramid_plugin.json_uuid_adapter(uuid1, None)
        self.assertEqual(result, '1804f59c-fdcd-11e8-8602-9cb6d0dde65d')
