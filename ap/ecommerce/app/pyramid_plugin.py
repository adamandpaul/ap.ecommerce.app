# -*- coding: utf-8 -*-

from ap.ecommerce.app import utils
from datetime import date
from datetime import datetime
from uuid import UUID

import logging
import pyramid.renderers
import pyramid_mailer


logger = logging.getLogger('ap.ecommerce.app')


def root_factory(request):
    """Return a new root"""
    return request.site


def get_user_uuid(request):
    """Return the current user_uuid"""
    user_uuids = []
    for principal in request.effective_principals:
        principal_components = principal.split(':', 1)
        ptype = principal_components[0]
        if ptype == 'user_uuid':
            assert len(principal_components) == 2
            pvalue = principal_components[1]
            user_uuids.append(pvalue)
    if len(user_uuids) == 1:
        return user_uuids[0]
    elif len(user_uuids) == 0:
        return None
    else:
        raise Exception('Can not retreive a user_uuid')


def get_user_display_name(request):
    """Get the user's display name"""
    user_uuid = request.user_uuid
    if user_uuid is not None:
        user_display_name = request.site.get_user_display_name(user_uuid)
        return user_display_name or user_uuid
    else:
        return request.authenticated_userid  # return the pyramid authenticated user id


def json_datetime_adapter(obj, request):
    """Adapt datetime to JSON"""
    return obj.isoformat()


def json_uuid_adapter(obj, request):
    return str(obj)


toolbar_html_template = """\
<link rel="stylesheet" type="text/css" href="%(css_path)s">
<style>
#pDocs {
    width: 40px;
    background: #A72D37;
    opacity: 0.8;
    color: white;
    border-left: 1px solid white;
    border-right: 1px solid ##A72D37;
    border-bottom: 1px solid white;
    display: block;
    text-align: center;
    font-size: 10px;
    font-weight: bold;
    padding: 7px 0;
    font-family: serif;
}
#pDocs:hover {
    text-decoration: none;
    opacity: 1.0;
}
</style>
<div id="pDebug">
    <div %(button_style)s id="pDebugToolbarHandle">
        <a title="Show Toolbar" id="pShowToolBarButton"
           href="%(toolbar_url)s" target="pDebugToolbar">&#171;</a>
        <a title="Documentation" id="pDocs" href="/++docs++/">DOC</a>
    </div>
</div>
"""


def monkey_patch_pyramid_debugtoolbar_toolbar_toolbar_html_template():
    import pyramid_debugtoolbar.toolbar
    pyramid_debugtoolbar.toolbar.toolbar_html_template = toolbar_html_template


def includeme(config):
    """Pyramid include hook"""

    # Monkey patches
    monkey_patch_pyramid_debugtoolbar_toolbar_toolbar_html_template()

    # Set root factory
    config.set_root_factory(root_factory)

    # Figure out develpment or production mode
    settings = config.get_settings()
    is_development = utils.yesish(settings['ap.ecommerce.is_development'])
    config.registry['is_development'] = is_development
    if is_development:
        logger.debug('Running in development mode')
    else:
        assert 'pyramid_debugtoolbar' not in settings.get('pyramid.includes', ''), 'Can not run in non development mode with pyramid_debugtoolbar'
        logger.debug('Running in production mode')

    # Add the JSON adapter for datetime
    json_renderer = pyramid.renderers.JSON()
    json_renderer.add_adapter(datetime, json_datetime_adapter)
    json_renderer.add_adapter(date, json_datetime_adapter)
    json_renderer.add_adapter(UUID, json_uuid_adapter)
    config.add_renderer('json', json_renderer)

    # Include base plugins
    config.include('pyramid_exclog')
    config.include('pyramid_zcml')
    config.include('pyramid_chameleon')
    config.add_settings({'tm.manager_hook': 'pyramid_tm.explicit_manager'})
    config.include('pyramid_tm')
    if is_development:
        mailer = pyramid_mailer.mailer.DummyMailer()
        pyramid_mailer._set_mailer(config, mailer)
    else:
        config.include('pyramid_mailer')

    # Add methods to requests
    config.add_request_method(get_user_uuid,
                              'user_uuid',
                              reify=True)
    config.add_request_method(get_user_display_name,
                              'user_display_name',
                              reify=True)

    # Configure documentation dir (if exists)
    if settings.get('ap.ecommerce.docs', None) is not None:
        if is_development:
            config.add_static_view(name='++docs++',
                                   path=settings['ap.ecommerce.docs'],
                                   cache_max_age=5)
        else:
            config.add_static_view(name='++docs++',
                                   path=settings['ap.ecommerce.docs'],
                                   permission='admin-docs',
                                   cache_max_age=300)

    # Configure app componnets
    config.include('.database')
    config.include('.security')
    config.include('.rendering')
    config.include('.api')
    config.include('.browser')
    config.include('.browser_admin')
