# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup


setup(name='ap.ecommerce.app',
      version='2.0.dev1',
      description='AP Ecommerce App',
      long_description = open('README.rst').read(),
      classifiers=['Programming Language :: Python'],
      keywords='',
      author='Adam & Paul',
      author_email='',
      url='http://adamandpaul.biz',
      license='gpl',
      packages=find_packages(),
      namespace_packages=['ap', 'ap.ecommerce'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',

          # Pyramid Deps
          'pyramid',
          'pyramid_chameleon',
          'pyramid_debugtoolbar',
          'pyramid_zcml',
          'waitress',
          'WebTest >= 1.3.1',  # py3 compat
          'pytest',
          'pytest-cov',

          # AP Ecommerce Deps
          'psycopg2-binary',
          'bcrypt',
          'lxml',
          'pyramid_exclog',
          'pyramid_mailer',
          'pyramid_multiauth',
          'pyramid_nacl_session',
          'pyramid_tm',
          'python-dateutil',
          'sqlalchemy >= 1.3.2',
          'sqlalchemy-utils',
          'velruse',
          'zope.sqlalchemy',
          'WTForms',
          'plone.testing',
          'pyjwt',
          'contextplus',

      ],
      entry_points="""
      [paste.app_factory]
      main = ap.ecommerce.app.wsgi_app:main
      """,
      )
